import enum
from operator import attrgetter
from typing import Dict
from cpalgnodes.algnode import SimpleAlgNode
from cpalgnodes.componentfacade import PublicTool
from cpalganalysis.nodeconfigblock import NodeConfigBlock
from cpalgnodes.selection import ScaleFactorAlg, SelectionAlgNode
from cpalganalysis.objectconfigblock import ObjectConfigBlock, ObjectDefinitionConfigBlock
from cpalgnodes.utils import DataType


class TauTruthMatchingAlg(SimpleAlgNode):
    """Performs truth matching with the offline taus"""

    def __init__(self, container: str, name: str = None, **kwargs):
        """Create the algorithm

        Parameters
        ----------
        container: str
            Nickname of the container to create
        name: str
            The name for the algorithm
        **kwargs
            Any other kwargs are forwarded to the superclass
        """
        if name is None:
            name = f"{container}TruthMatchingAlg"
        super().__init__(
            type="CP::TauTruthMatchingAlg",
            name=name,
            container_property="taus",
            container=container,
            produces_aux={"truthParticleLink"},
            **kwargs,
        )
        self.setPrivateTool(
            "matchingTool", "TauAnalysisTools::TauTruthMatchingTool", WriteTruthTaus=1
        )


class TauSmearingAlg(SimpleAlgNode):
    """Algorithm to calibrate taus"""

    def __init__(
        self,
        container: str,
        dataType: DataType,
        name: str = None,
        calib_dep: str = "__CALIBRATION__",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the container to run over
        dataType: DataType
            The data type being run over
        name: str
            The name of the algorithm
        calib_dep: str
            A dummy dependency to force other algorithms to depend on the calibration
        **kwargs
        """
        if name is None:
            name = f"{container}SmearingAlg"
        self._calib_dep = calib_dep
        super().__init__(
            type="CP::TauSmearingAlg",
            name=name,
            container_property="taus",
            container=container,
            container_out_property="tausOut",
            produces_aux={attrgetter("_calib_dep")},
            requires_aux=set() if dataType.is_data else {"truthParticleLink"},
            **kwargs,
        )
        self.setPrivateTool("smearingTool", "TauAnalysisTools::TauSmearingTool")


class TauSelectionAlg(SelectionAlgNode):
    """Tau ID selection alg"""

    class WorkingPoint(enum.Enum):
        Baseline = enum.auto()
        NoID = enum.auto()
        VeryLoose = enum.auto()
        Loose = enum.auto()
        Medium = enum.auto()
        Tight = enum.auto()

    def __init__(
        self,
        container: str,
        wp: WorkingPoint,
        name: str = None,
        legacy: bool = False,
        calib_dep: str = "__CALIBRATION__",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the container
        wp: WorkingPoint
            The working point to use
        name: str
            The name of the algorithm
        legacy: bool
            Whether to use the legacy recommendations
        calib_dep: str
            Dummy dependency to make this run after calibration
        **kwargs
            Extra kwargs are passed to the superclass
        """
        legacy_str = "Legacy" if legacy else ""
        if name is None:
            name = f"{container}{wp.name}{legacy_str}IDSelectionAlg"
        self._calib_dep = calib_dep
        super().__init__(
            name=name,
            container=container,
            selection_name=f"{wp.name}ID{legacy_str}",
            selection_count=6,
            requires_aux={attrgetter("_calib_dep")},
            **kwargs,
        )
        self["selectionTool"] = self.create_selection_tool(container, wp, legacy)

    @classmethod
    def create_selection_tool(
        self, container: str, wp: WorkingPoint, legacy: bool = False, name: str = None
    ):
        """Create the public selection tool

        Parameters
        ----------
        container: str
            The nickname of the container
        wp: WorkingPoint
            The working point to use
        legacy: bool
            Whether to use the legacy recommendations
        name: str
            The name of the tool
        """
        legacy_str = "Legacy" if legacy else ""
        # NB: This method is necessary as the same tool has to be used by the efficiency corrections algorithm
        if legacy:
            pattern = "TauAnalysisAlgorithms/tau_selection_{}_legacy.conf"
        else:
            pattern = "TauAnalysisAlgorithms/tau_selection_{}.conf"
        if name is None:
            name = f"{container}{wp.name}{legacy_str}IDSelectionTool"
        # TODO: Make this a private tool?
        return PublicTool(
            "TauAnalysisTools::TauSelectionTool",
            name,
            ConfigPath=pattern.format(wp.name.lower()),
        )

    class Config(NodeConfigBlock):
        """The tau ID selection configuration"""

        def __init__(self):
            """Create the configuration block"""
            super().__init__()
            self.add_enum_option(
                "ID",
                TauSelectionAlg.WorkingPoint,
                help="The tau ID working point to use",
                set_from_single_value=True,
            )
            self.add_option(
                "Legacy",
                type=bool,
                default=False,
                help="Whether to use the legacy working points",
            )

        def _create_nodes(
            self,
            metadata: Dict,
            container: str,
            calib_dep: str = "__CALIBRATION__",
            **kwargs,
        ):
            nodes = {
                "Sel": TauSelectionAlg(
                    container=container,
                    wp=self["ID"],
                    legacy=self["Legacy"],
                    calib_dep=calib_dep,
                )
            }
            if metadata["dataType"].is_simulation:
                nodes["SF"] = TauEfficiencyCorrectionAlg(
                    container=container,
                    wp=self["ID"],
                    legacy=self["Legacy"],
                    calib_dep=calib_dep,
                )
            return nodes


class TauEfficiencyCorrectionAlg(ScaleFactorAlg):
    """Scale factor correciton alg"""

    def __init__(
        self,
        container: str,
        wp: TauSelectionAlg.WorkingPoint = None,
        legacy: bool = False,
        name: str = None,
        calib_dep="__CALIBRATION__",
        **kwargs,
    ):
        """Create the scale factor algorithm

        Parameters
        ----------
        container: str
            The nickname of the container
        wp: TauSelectionAlg.WorkingPoint
            The working point to calculate the efficiency for
        legacy: bool
            Whether to use legacy recommendations
        name: str
            The name of the algorithm
        calib_dep: str
            Dummy dependency to make this depend on the calibration
        **kwargs
            Extra kwargs are forwarded to the super class
        """
        legacy_str = "Legacy" if legacy else ""
        if name is None:
            name = f"{container}{wp.name}{legacy_str}EfficiencyCorrectionsAlg"
        self._calib_dep = calib_dep
        super().__init__(
            type="CP::TauEfficiencyCorrectionsAlg",
            name=name,
            container=container,
            container_property="taus",
            sf_name=f"{wp.name}ID{legacy_str}SF",
            requires_aux={attrgetter("_calib_dep")},
            **kwargs,
        )
        self.setPrivateTool(
            "efficiencyCorrectionsTool",
            "TauAnalysisTools::TauEfficiencyCorrectionsTool",
            TauSelectionTool=TauSelectionAlg.create_selection_tool(
                container, wp, legacy
            ),
        )


class TauDefinitionConfigBlock(ObjectDefinitionConfigBlock):
    """Config block for a single tau selection"""

    def __init__(self):
        """Create the config block"""
        super().__init__(
            use_cluster_eta=False,
            add_crack_veto_opt=False,
            order=["Kinematic", "ID"],
        )
        self.add_subconfig("ID", TauSelectionAlg.Config())


class TauConfigBlock(ObjectConfigBlock):
    """Config block for taus"""

    def __init__(self):
        """Create the configuration"""
        super().__init__(
            object_type="tau",
            default_input_container="TauJets",
            default_subconfig_type=TauDefinitionConfigBlock,
        )

    def _create_nodes(
        self,
        metadata: Dict,
        container: str,
        calib_dep: str,
        **kwargs,
    ):
        nodes = {
            "Calibration": TauSmearingAlg(
                container=container,
                dataType=metadata["dataType"],
                calib_dep=calib_dep,
            )
        }
        if metadata["dataType"].is_simulation:
            nodes["TruthMatching"] = TauTruthMatchingAlg(container)
        nodes.update(
            super()._create_nodes(
                metadata, container=container, calib_dep=calib_dep, **kwargs
            )
        )
        return nodes

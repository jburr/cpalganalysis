import enum
from operator import attrgetter
from typing import Dict


from cpalgnodes.algnode import SimpleAlgNode
from cpalganalysis.nodeconfigblock import NodeConfigBlock
from cpalganalysis.core import LeptonTrackSelectionAlg
from cpalganalysis.objectconfigblock import (
    ObjectConfigBlock,
    ObjectDefinitionConfigBlock,
)
from cpalgnodes.scheduler import Scheduler
from cpalgnodes.utils import DataType
from cpalgnodes.selection import ScaleFactorAlg, SelectionAlgNode

# TODO
# - set 2 station high pT smearing when using highPt working point?


class MuonEfficiencyScaleFactorAlg(ScaleFactorAlg):
    """Algorithm to calculate scale factors"""

    def __init__(
        self,
        wp: str,
        container: str,
        name: str = None,
        sf_name: str = None,
        calib_dep="__CALIBRATION__",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        wp: str
            The name of the working point. Add 'Iso' to the end of isolation working points and use
            'BadMuon' for the bad muon selection
        """
        if name is None:
            name = f"{container}{wp}EfficiencyScaleFactorAlg"
        if sf_name is None:
            sf_name = f"{wp}SF"
        self._calib_dep = calib_dep
        super().__init__(
            type="CP::MuonEfficiencyScaleFactorAlg",
            name=name,
            container_property="muons",
            container=container,
            sf_name=sf_name,
            requires_aux={attrgetter("_calib_dep")},
            **kwargs,
        )
        self.setPrivateTool(
            "efficiencyScaleFactorTool",
            "CP::MuonEfficiencyScaleFactorAlg",
            WorkingPoint=wp,
        )


class MuonIDSelectionAlg(SelectionAlgNode):
    """Muon ID Selection"""

    class WorkingPoint(enum.Enum):
        Tight = 0
        Medium = 1
        Loose = 2
        VeryLoose = 3
        HighPt = 4
        LowPtEfficiency = 5

    def __init__(
        self,
        wp: WorkingPoint,
        name: str = None,
        container: str = "muons",
        isRun3Geo: bool = True,
        calib_dep="__CALIBRATION__",
        **properties,
    ):
        """Create the muon ID selection node

        Parameters
        ----------
        wp: WorkingPoint
            The working point to use
        name: str
            The name of the algorithm to use
        container: str
            The nickname of the container to read
        calib_dep: str
            The name of a dummy dependency to ensure that this runs after the calibration
        """
        if name is None:
            name = f"{container}{wp.name}IDSelectionAlg"
        self._calib_dep = calib_dep
        super().__init__(
            type="CP::MuonSelectionAlgV2",
            name=name,
            container_property="muons",
            container=container,
            selection_name=wp.name + "ID",
            selection_count=4,
            # Don't decorate the bad muon veto from this alg (we have a dedicated one for that)
            badMuonVetoDecoration="",
            requires_aux={attrgetter("_calib_dep")},
            **properties,
        )
        self.setPrivateTool(
            "selectionTool",
            "CP::MuonSelectionTool",
            MuQuality=wp.value,
            IsRun3Geo=isRun3Geo,
        )

    class Config(NodeConfigBlock):
        """ConfigBlock to create the ID selection"""

        def __init__(self):
            """Create the configuration

            Parameters
            ----------
            parent: ConfigBlock
                The parent configuration of this block
            """
            super().__init__()
            self.add_enum_option(
                "ID",
                enum_type=MuonIDSelectionAlg.WorkingPoint,
                help="The ID working point",
                set_from_single_value=True,
            )

        def _create_nodes(
            self,
            metadata: Dict,
            container: str,
            calib_dep: str = "__CALIBRATION__",
            **kwargs,
        ):
            # TODO: What's the best way to disambiguate a 'big-R' LHC Run number and a 'small-R' run number
            nodes = {
                "Sel": MuonIDSelectionAlg(
                    self["ID"],
                    container=container,
                    calib_dep=calib_dep,
                    isRun3Geo=metadata["LHCRun"] == 3,
                )
            }
            if metadata["dataType"].is_simulation:
                nodes["SF"] = MuonEfficiencyScaleFactorAlg(
                    self["ID"].name,
                    container=container,
                    calib_dep=calib_dep,
                    sf_name=self["ID"].name + "IDSF",
                )
            return nodes


class MuonIsolationSelectionAlg(SelectionAlgNode):
    """Algorithm to select muons based on isolation"""

    class WorkingPoint(enum.Enum):
        PflowLoose_VarRad = enum.auto()
        PflowTight_VarRad = enum.auto()
        Loose_VarRad = enum.auto()
        Tight_VarRad = enum.auto()

    def __init__(
        self,
        wp: WorkingPoint,
        name: str = None,
        container: str = "muons",
        calib_dep: str = "__CALIBRATION__",
        **properties,
    ):
        """Create the muon isolation selection node

        Parameters
        ----------
        wp: WorkingPoint
            The working point to use
        name: str
            The name of the algorithm to use
        container: str
            The nickname of the container to read
        calib_dep: str
            The name of a dummy dependency to ensure that this runs after the calibration
        """
        if name is None:
            name = f"{container}{wp.name}IsolationSelectionAlg"
        self._calib_dep = calib_dep
        super().__init__(
            type="CP::MuonIsolationAlg",
            name=name,
            container_property="muons",
            container=container,
            selection_name=wp.name + "Iso",
            selection_property="isolationDecoration",
            selection_count=1,
            requires_aux={attrgetter("_calib_dep")},
            **properties,
        )
        self.setPrivateTool(
            "isolationTool", "CP::IsolationSelectionTool", MuonWP=wp.name
        )

    class Config(NodeConfigBlock):
        """Config block for muon isolation selection"""

        def __init__(self):
            """Create the configuration"""
            super().__init__()
            self.add_enum_option(
                "Iso",
                MuonIsolationSelectionAlg.WorkingPoint,
                help="The isolation working point to use",
                set_from_single_value=True,
            )

        def _create_nodes(
            self, metadata: Dict, container: str, calib_dep: str, **kwargs
        ):
            nodes = {
                "Sel": MuonIsolationSelectionAlg(
                    self["Iso"],
                    container=container,
                    calib_dep=calib_dep,
                )
            }
            if metadata["dataType"].is_simulation:
                nodes["SF"] = MuonEfficiencyScaleFactorAlg(
                    self["Iso"].name + "Iso",
                    container=container,
                    calib_dep=calib_dep,
                    sf_name=self["Iso"].name + "IsoSF",
                )
            return nodes


class MuonDefinitionConfigBlock(ObjectDefinitionConfigBlock):
    """Config block for a single muon selection"""

    def __init__(self):
        """Create the config block"""
        super().__init__(
            use_cluster_eta=False,
            order=["Track", "Kinematic", "ID", "Iso"],
        )
        self.add_subconfig("ID", MuonIDSelectionAlg.Config())
        self.add_subconfig("Iso", MuonIsolationSelectionAlg.Config())
        self.add_subconfig("Track", LeptonTrackSelectionAlg.Config())
        self.get_option_data("Track/D0Sig").default = 3
        self.get_option_data("Track/Z0SinTheta").default = "0.5 mm"


class BadMuonSelectionAlg(SelectionAlgNode):
    """Algorithm to select bad muons"""

    def __init__(
        self,
        container: str,
        selection_name: str = "BadMuon",
        calib_dep="__CALIBRATION__",
        name=None,
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the input container
        selection_name: str
            The selection name to set
        calib_dep: str
            Dummy dependency to ensure that this runs after calibration
        name: str
            The name of the algorithm
        **kwargs
            Extra kwargs are passed to the superclass
        """
        if name is None:
            name = f"{container}BadMuonSelectionAlg"
        self._calib_dep = calib_dep
        super().__init__(
            type="CP::MuonSelectionAlgV2",
            name=name,
            container=container,
            container_property="muons",
            selection_name=selection_name,
            selection_property="badMuonVetoDecoration",
            # We have to provide this. I will provide a fix
            selectionDecoration="badMuonVetoAlgDummy",
            requires_aux={attrgetter("_calib_dep")},
            **kwargs,
        )
        self.setPrivateTool("selectionTool", "CP::MuonSelectionTool")


class MuonCalibrationAlg(SimpleAlgNode):
    """Algorithm to calibrate muons"""

    class CalibrationMode(enum.Enum):
        """The different calibration mode options"""

        correctData_CB = 0
        correctData_IDMS = 1
        notCorrectData_IDMS = 2

    def __init__(
        self,
        container: str,
        mode: CalibrationMode,
        dataType: DataType,
        name: str = None,
        isRun3Geo: bool = True,
        calib_dep: str = "__CALIBRATION__",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            Nickname of the container
        mode: CalibrationMode
            The calibration mode to use (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MCPAnalysisGuidelinesMC16#Details_on_using_the_MuonCalibra)
        dataType: DataType
            The type of data being run over
        name: Optional[str]
            The name of the algorithm
        calib_dep: str
            The name of a dummy dependency to let other algorithms depend on the calibration
        **kwargs
            Other kwargs are passed to the superclass
        """
        if name is None:
            name = f"{container}CalibrationAndSmearingAlg"
        self._calib_dep = calib_dep
        super().__init__(
            type="CP::MuonCalibrationAndSmearingAlg",
            name=name,
            container_property="muons",
            container_out_property="muonsOut",
            container=container,
            produces_aux={attrgetter("_calib_dep")},
            **kwargs,
        )
        self._isMC = dataType.is_simulation
        self.setPrivateTool(
            "calibrationAndSmearingTool",
            "CP::MuonCalibTool",
            calibMode=mode.value,
            IsRun3Geo=isRun3Geo,
        )

    @property
    def requires_aux(self):
        requires = super().requires_aux
        if self._isMC and self["calibrationAndSmearingTool"].get(
            "useRandomRunNumber", False
        ):
            requires["EventInfo"] = {"RandomRunNumber"}
        return requires


class MuonConfigBlock(ObjectConfigBlock):
    """Config block for muons"""

    def __init__(self):
        super().__init__(
            object_type="muon",
            default_input_container="Muons",
            default_subconfig_type=MuonDefinitionConfigBlock,
        )
        self.add_enum_option(
            "CalibrationMode",
            enum_type=MuonCalibrationAlg.CalibrationMode,
            help="The calibration mode to use (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MCPAnalysisGuidelinesMC16#Details_on_using_the_MuonCalibra)",
        )
        self.add_option(
            "BadMuonDecoration",
            default="BadMuon",
            help="The decoration to store bad muon information. Set to None to disable",
        )

    def _create_nodes(
        self,
        metadata: Dict,
        container: str,
        calib_dep: str,
        **kwargs,
    ):
        nodes = {
            "Calibration": MuonCalibrationAlg(
                container=container,
                mode=self["CalibrationMode"],
                dataType=metadata["dataType"],
                isRun3Geo=metadata["LHCRun"] == 3,
                calib_dep=calib_dep,
            )
        }
        if self["BadMuonDecoration"]:
            nodes["BadMuon"] = {
                "Sel": BadMuonSelectionAlg(
                    container=container,
                    selection_name=self["BadMuonDecoration"],
                    calib_dep=calib_dep,
                )
            }
            # TODO: Preselection on the bad muon SF algorithm to only include objects passing the bad muon?
            if metadata["dataType"].is_simulation:
                nodes["BadMuon"]["SF"] = MuonEfficiencyScaleFactorAlg(
                    wp="BadMuon",
                    container=container,
                    calib_dep=calib_dep,
                    sf_name=self["BadMuonDecoration"] + "SF",
                )
        nodes.update(
            super()._create_nodes(
                metadata, container=container, calib_dep=calib_dep, **kwargs
            )
        )
        return nodes


if __name__ == "__main__":
    muons = MuonConfigBlock(None)
    muons.set_from_key("AnalysisMuons")
    muons["CalibrationMode"] = "correctData_IDMS"
    muons["baseline"] = {
        "MinPt": 5e3,
        "MaxEta": 2.5,
        "ID": "Loose",
        "Track": {
            "D0Sig": 3,
            "Z0SinTheta": "0.5 mm",
        },
    }
    muons.validate()
    metadata = {"dataType": DataType.FullSim}
    nodes = muons.create_nodes(metadata)
    from cpalgnodes.scheduler import Scheduler

    scheduler = Scheduler()
    scheduler += nodes

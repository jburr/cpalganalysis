"""Algorithms and configurations for egamma objects"""

import enum
from operator import attrgetter
from typing import Dict
from cpalgnodes.algnode import SimpleAlgNode
from cpalganalysis.objectconfigblock import ObjectConfigBlock
from cpalgnodes.selection import SelectionAlgNode
from cpalgnodes.utils import DataType


class EgammaCalibrationAndSmearingAlg(SimpleAlgNode):
    """Algorithm to calibrated egamma objects"""

    def __init__(
        self,
        container: str,
        dataType: DataType,
        name: str = None,
        calib_dep="__CALIBRATION__",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the container to read
        dataType: DataType
            The data type being run over
        name: Optional[str]
            The name of the algorithm to set
        calib_dep: str
            A dummy dependency to allow other nodes to depend on the output of this one
        **kwargs
            Other kwargs are forwarded to the superclass
        """
        if name is None:
            name = f"{container}CalibrationAndSmearingAlg"
        self._calib_dep = calib_dep
        super().__init__(
            type="CP::EgammaCalibrationAndSmearingAlg",
            name=name,
            container_property="egammas",
            container=container,
            container_out_property="egammasOut",
            produces_aux={attrgetter("_calib_dep")},
            **kwargs,
        )
        self.setPrivateTool(
            "calibrationAndSmearingTool",
            "CP::EgammaCalibrationAndSmearingTool",
            ESModel="es2018_R21_v0",
            decorrelationModel="1NP_v1",
            useAFII=1 if dataType == DataType.AFII else 0,
        )
        self._isMC = dataType.is_simulation

    @property
    def requires_aux(self):
        requires = super().requires_aux
        if self._isMC:
            requires["EventInfo"] = {"RandomRunNumber"}
        return requires


class EgammaIsolationCorrectionAlg(SimpleAlgNode):
    """Algorithm to calculate corrections to the egamma isolation variables"""

    def __init__(
        self,
        container: str,
        dataType: DataType,
        name: str = None,
        isocorr_dep="__ISOCORR__",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the container
        dataType: DataType
            The data type being run over
        name: str
            The name of the algorithm
        isocorr_dep: str
            A dummy dependency to allow other nodes to depend on the output of this one
        **kwarg
            Extra kwargs are passed to the superclass
        """
        if name is None:
            name = f"{container}IsolationCorrectionAlg"
        self._isocorr_dep = isocorr_dep
        super().__init__(
            type="CP::EgammaIsolationCorrectionAlg",
            name=name,
            container_property="egammas",
            container=container,
            container_out_property="egammasOut",
            produces_aux={attrgetter("_isocorr_dep")},
            **kwargs,
        )
        self.setPrivateTool(
            "isolationCorrectionTool",
            "CP::IsolationCorrectionTool",
            IsMC=dataType.is_simulation,
        )


class EgammaIsolationSelectionAlg(SelectionAlgNode):
    """Algorithm to select electrons/photons based on isolation"""

    def __init__(
        self,
        container: str,
        wp: str,
        tool_prop: str,
        name: str = None,
        calib_dep: str = "__CALIBRATION__",
        isocorr_dep: str = "__ISOCORR__",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the container to select on
        wp: str
            The working point to use
        tool_prop: str
            The tool property to set (ElectronWP or PhotonWP)
        name: str
            The name of the algorithm
        calib_dep: str
            Dummy calibration dependency to force this to run after calibration
        isocorr_dep: str
            Dummy correction dependency to force this to run after the isolation correction
        **kwargs
            Extra kwargs are passed to the superclass
        """
        if name is None:
            name = f"{container}{wp}IsolationSelectionAlgorithm"
        self._calib_dep = calib_dep
        self._isocorr_dep = isocorr_dep
        super().__init__(
            name=name,
            type="CP::EgammaIsolationSelectionAlg",
            container_property="egammas",
            container=container,
            selection_name=f"{wp}Iso",
            selection_count=1,
            requires_aux={attrgetter("_calib_dep"), attrgetter("_isocorr_dep")},
            **kwargs,
        )
        tool = self.setPrivateTool("selectionTool", "CP::IsolationSelectionTool")
        tool[tool_prop] = wp


class EgammaObjectQualityAlg(SelectionAlgNode):
    """Algorithm to select electrons/photons based on object quality"""

    def __init__(
        self,
        container: str,
        mask: int,
        selection_name: str = "goodOQ",
        name: str = None,
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the container to select on
        mask: int
            The integer mask to use
        name: str
            The name of the algorithm
        **kwargs
            Extra kwargs are passed to the superclass
        """
        if name is None:
            name = f"{container}ObjectQualitySelectionAlgorithm"
        super().__init__(
            name=name,
            container=container,
            selection_name=selection_name,
            selection_count=1,
            **kwargs,
        )
        self.setPrivateTool(
            "selectionTool", "CP::EgammaIsGoodOQSelectionTool", Mask=mask
        )


class EgammaConfigBlock(ObjectConfigBlock):
    """Base object config block for egamma objects"""

    def _create_nodes(
        self,
        metadata: Dict,
        container: str,
        calib_dep: str = "__CALIBRATION__",
        **kwargs,
    ):
        nodes = {
            "Calibration": EgammaCalibrationAndSmearingAlg(
                container=container, dataType=metadata["dataType"], calib_dep=calib_dep
            ),
        }
        nodes.update(
            super()._create_nodes(
                metadata,
                container=container,
                calib_dep=calib_dep,
                **kwargs,
            )
        )
        return nodes

"""Algorithms and configurations for electrons"""

import enum
from operator import attrgetter
from typing import Any, Dict
from cpalganalysis.nodeconfigblock import NodeConfigBlock
from configblock.utils import wrap_deferred_func, find_option_on_block_of_type
from cpalganalysis.core import LeptonTrackSelectionAlg
from cpalganalysis.egamma import (
    EgammaConfigBlock,
    EgammaIsolationSelectionAlg,
    EgammaObjectQualityAlg,
    EgammaIsolationCorrectionAlg,
)
from cpalganalysis.objectconfigblock import (
    ObjectDefinitionConfigBlock,
    ObjectSelectionConfigBlock,
)
from cpalgnodes.selection import ScaleFactorAlg, SelectionAlgNode
from cpalgnodes.utils import DataType

from xAODEgamma.xAODEgammaParameters import xAOD


class ElectronIDSelectionAlg(SelectionAlgNode):
    """Select electrons based on ID"""

    class WorkingPoint(enum.Enum):
        """Electron ID working points"""

        # NB: These are deliberately arranged in order of increasing tightness within their
        # categories
        LooseDNN = enum.auto()
        MediumDNN = enum.auto()
        TightDNN = enum.auto()
        VeryLooseLLH = enum.auto()
        LooseLLH = enum.auto()
        LooseAndBLayerLLH = enum.auto()
        MediumLLH = enum.auto()
        TightLLH = enum.auto()

        @property
        def is_dnn(self) -> bool:
            """Whether this is a DNN working point"""
            return "DNN" in self.name

        @property
        def is_llh(self) -> bool:
            """Whether this is a LLH working point"""
            return "LLH" in self.name

        @property
        def df_name(self) -> str:
            """The name of this WP in the derivation framework"""
            if self.is_llh:
                if self == ElectronIDSelectionAlg.WorkingPoint.LooseAndBLayerLLH:
                    suffix = "LHLooseBL"
                else:
                    suffix = "LH" + self.name.replace("LLH", "")
            else:
                suffix = "DNN" + self.name.replace("DNN", "")
            return "DFCommonElectrons" + suffix

        @property
        def sel_tool_name(self) -> str:
            """The name of this WP in the relevant selection tool"""
            if self == ElectronIDSelectionAlg.WorkingPoint.LooseAndBLayerLLH:
                return "LooseBLLHElectron"
            elif self.is_llh:
                return self.name.replace("LLH", "LH") + "Electron"
            else:
                return self.name

    @classmethod
    def default_selection_name(cls, wp: WorkingPoint) -> str:
        """Get the default selection name for a working point"""
        return wp.name + "ID"

    def __init__(
        self,
        container: str,
        wp: WorkingPoint,
        recompute_score: bool = False,
        name: str = None,
        selection_name: str = None,
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the input container
        wp: WorkingPoint
            The working point to use
        recompute_likelihood: bool
            If True, recalculate the likelihood value, otherwise use the DF-level decorations
        name: str
            The name of the algorithm
        selection_name: str
            The name of the selection to set
        **kwargs
            Extra kwargs are forwarded to the superclass
        """
        if selection_name is None:
            selection_name = self.default_selection_name(wp)
        if name is None:
            name = f"{container}{selection_name}SelectionAlg"
        super().__init__(
            name=name,
            container=container,
            selection_name=selection_name,
            **kwargs,
        )
        if not recompute_score:
            self.setPrivateTool(
                "selectionTool",
                "CP::AsgFlagSelectionTool",
                selectionFlags=[wp.df_name],
            )
            self.selection_count = None
        elif wp.is_llh:
            self.setPrivateTool(
                "selectionTool",
                "AsgElectronLikelihoodTool",
                primaryVertexContainer="PrimaryVertices",
                WorkingPoint=wp.sel_tool_name,
            )
            self.selection_count = 7
        else:
            self.setPrivateTool(
                "selectionTool",
                "AsgElectronSelectorTool",
                WorkingPoint=wp.sel_tool_name,
            )
            self.selection_count = 6

        self._wp = wp

    @property
    def requires_aux(self):
        requires = super().requires_aux
        if self["selectionTool"].type == "AsgElectronLikelihoodTool":
            requires.setdefault(self["selectionTool.primaryVertexContainer"], set())
        return requires

    class Config(ObjectSelectionConfigBlock):
        def __init__(self):
            super().__init__(
                default_sel_name=wrap_deferred_func(
                    ElectronIDSelectionAlg.default_selection_name, rename={"wp": "ID"}
                ),
                defines_sf=True,
            )
            self.add_enum_option(
                "ID",
                ElectronIDSelectionAlg.WorkingPoint,
                set_from_single_value=True,
                help="The ID working point to use",
            )
            # TODO: Prefer to deduce this from file type?
            self.add_option(
                "RecomputeScore",
                False,
                type=bool,
                help="Whether to calculate the likelihood/DNN score or use the derivation flags",
            )

        def _create_nodes(self, metadata: Dict, container: str, **kwargs):
            nodes = {
                "Sel": ElectronIDSelectionAlg(
                    container,
                    wp=self["ID"],
                    recompute_score=self["RecomputeScore"],
                )
            }
            if metadata["dataType"].is_simulation:
                nodes["SF"] = ElectronEfficiencyCorrectionAlg(
                    container,
                    dataType=metadata["dataType"],
                    mode=ElectronEfficiencyCorrectionAlg.Mode.ID,
                    id_wp=self["ID"],
                )
            return nodes


class ElectronIsolationSelectionAlg(EgammaIsolationSelectionAlg):
    """Algorithm to select electrons based on isolation"""

    class WorkingPoint(enum.Enum):
        HighPtCaloOnly = enum.auto()
        Tight_VarRad = enum.auto()
        Loose_VarRad = enum.auto()
        TightTrackOnly_VarRad = enum.auto()
        TightTrackOnly_FixedRad = enum.auto()
        PflowTight_FixedRad = enum.auto()
        PflowTight = enum.auto()
        PflowLoose_FixedRad = enum.auto()
        PflowLoose = enum.auto()

    def __init__(
        self,
        container: str,
        wp: WorkingPoint,
        name: str = None,
        calib_dep: str = "__CALIBRATION__",
        isocorr_dep: str = "__ISOCORR__",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the container to select on
        wp: WorkingPoint
            The working point to use
        name: str
            The name of the algorithm
        calib_dep: str
            Dummy calibration dependency to force this to run after calibration
        isocorr_dep: str
            Dummy correction dependency to force this to run after the isolation correction
        **kwargs
            Extra kwargs are passed to the superclass
        """
        super().__init__(
            container=container,
            wp=wp.name,
            tool_prop="ElectronWP",
            name=name,
            calib_dep=calib_dep,
            isocorr_dep=isocorr_dep,
            **kwargs,
        )

    class Config(NodeConfigBlock):
        """Config block for electron isolation selection"""

        def __init__(self):
            """Create the configuration"""
            super().__init__()
            self.add_enum_option(
                "Iso",
                ElectronIsolationSelectionAlg.WorkingPoint,
                help="The isolation working point to use",
                set_from_single_value=True,
            )
            self.add_enum_option(
                "IDForSF",
                ElectronIDSelectionAlg.WorkingPoint,
                default=find_option_on_block_of_type(
                    type=ElectronIDSelectionAlg.Config,
                    key="ID",
                    # Only search on the parent to avoid finding ID selections from other definitions
                    max_steps=1,
                ),
                help="The ID working point to use for calculating the SF (only needs to be set if it can't be deduced)",
                detail=10,
            )

        def _create_nodes(
            self,
            metadata: Dict,
            container: str,
            calib_dep: str,
            isocorr_dep: str = "__ISOCORR__",
            **kwargs,
        ):
            nodes = {
                "Sel": ElectronIsolationSelectionAlg(
                    container=container,
                    wp=self["Iso"],
                    calib_dep=calib_dep,
                    isocorr_dep=isocorr_dep,
                )
            }
            if metadata["dataType"].is_simulation:
                nodes["IsolationCorrection"] = EgammaIsolationCorrectionAlg(
                    container=container,
                    dataType=metadata["dataType"],
                    isocorr_dep=isocorr_dep,
                )
                nodes["SF"] = ElectronEfficiencyCorrectionAlg(
                    container=container,
                    dataType=metadata["dataType"],
                    mode=ElectronEfficiencyCorrectionAlg.Mode.Iso,
                    id_wp=self["IDForSF"],
                    iso_wp=self["Iso"],
                )
            return nodes


class ElectronEfficiencyCorrectionAlg(ScaleFactorAlg):
    """Algorithm to calculate scale factors for electron selections"""

    class Mode(enum.Enum):
        Reco = enum.auto()
        ID = enum.auto()
        Iso = enum.auto()
        ChargeID = enum.auto()

    def __init__(
        self,
        container: str,
        dataType: DataType,
        mode: Mode = Mode.Reco,
        id_wp: ElectronIDSelectionAlg.WorkingPoint = None,
        iso_wp: ElectronIsolationSelectionAlg.WorkingPoint = None,
        name: str = None,
        sf_name: str = None,
        correlation_model: str = "TOTAL",
        calib_dep: str = "__CALIBRATION__",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the container to select on
        dataType: DataType
            The type of data being run over
        mode: Mode
            The type of scale factor to calculate (Reco, ID or Iso)
        id_wp: ElectronIDSelectionAlg.WorkingPoint
            The name of the ID working point (for ID and Iso SFs)
        iso_wp: ElectronIsolationSelectionAlg.WorkingPoint
            The name of the isolation working point (for Iso SFs)
        name: str
            The name of the algorithm
        sf_name: str
            The name of the scale factor to create. Can be left as None in which case a sensible
            default is chosen
        calib_dep: str
            Dummy dependency to allow this to depend on the calibration
        **kwargs
            Any extra kwargs are passed to the superclass
        """
        wp_part = ""
        # Validate the provided options and build up the default name/sf name string
        if mode == ElectronEfficiencyCorrectionAlg.Mode.ID:
            if id_wp is None:
                raise ValueError("ID working point must be provided in the ID mode")
            wp_part = id_wp.name
        elif mode == ElectronEfficiencyCorrectionAlg.Mode.Iso:
            if id_wp is None or iso_wp is None:
                raise ValueError(
                    "Both ID and Iso working points must be provided in the Iso mode"
                )
            wp_part = id_wp.name + "_" + iso_wp.name
        if name is None:
            name = f"{container}{wp_part}{mode.name}EfficiencyCorrectionAlg"
        if sf_name is None:
            sf_name = f"{wp_part}{mode.name}SF_%SYS%"
        self._calib_dep = calib_dep
        super().__init__(
            type="CP::ElectronEfficiencyCorrectionAlg",
            name=name,
            container_property="electrons",
            container=container,
            sf_name=sf_name,
            requires_aux={attrgetter("_calib_dep")},
            **kwargs,
        )
        tool = self.setPrivateTool(
            "efficiencyCorrectionTool",
            "AsgElectronEfficiencyCorrectionTool",
            CorrelationModel=correlation_model,
            ForceDataType=dataType.value,
        )
        if mode == ElectronEfficiencyCorrectionAlg.Mode.Reco:
            tool["RecoKey"] = "Reconstruction"
        elif mode == ElectronEfficiencyCorrectionAlg.Mode.ID:
            tool["IdKey"] = id_wp.name
        elif mode == ElectronEfficiencyCorrectionAlg.Mode.Iso:
            tool["IdKey"] = id_wp.name
            tool["IsoKey"] = iso_wp.name


# TODO: The tool doesn't seem to use the WorkingPoint property at all
class ElectronChargeIDSelectionAlg(SelectionAlgNode):

    # TODO: enum?
    def __init__(self, container: str, wp: str, **kwargs):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the container to select on
        wp: str
            The name of the working point to use
        **kwargs
            Extra kwargs are passed onto the superclass
        """
        super().__init__(
            container=container,
            selection_name=wp + "ChargeID",
            selection_count=None,
            **kwargs,
        )
        self.setPrivateTool(
            "selectionTool",
            "AsgElectronChargeIDSelectorTool",
            TrainingFile="ElectronPhotonSelectorTools/ChargeID/ECIDS_20180731rel21Summer2018.root",
            WorkingPoint=wp,
        )

    class Config(NodeConfigBlock):
        def __init__(self):
            super().__init__()
            self.add_option(
                "ChargeID", help="The electron charge ID working point to use"
            )

        def _create_nodes(self, metadata: Dict, container: str, **kwargs):
            # TODO: Scale factors?
            return ElectronChargeIDSelectionAlg(container, wp=self["ChargeID"])


class ElectronDefinitionConfigBlock(ObjectDefinitionConfigBlock):
    """Config block for a single electron selection"""

    def __init__(self):
        """Create the config block"""
        super().__init__(
            use_cluster_eta=True,
            add_crack_veto_opt=True,
            order=["OQ", "Eta", "Track", "ID", "Pt", "Iso"],
        )
        self.add_option(
            "GoodOQ", type=bool, default=True, detail=10, help="Apply the quality cut"
        )
        self.add_subconfig("ID", ElectronIDSelectionAlg.Config())
        self.add_subconfig("Iso", ElectronIsolationSelectionAlg.Config())
        self.add_subconfig("Track", LeptonTrackSelectionAlg.Config())
        self.get_option_data("Track/D0Sig").default = 5
        self.get_option_data("Track/Z0SinTheta").default = "0.5 mm"

    def _create_selection_and_sf_nodes(self, metadata: Dict, container: str, **kwargs):
        extra = super()._create_selection_and_sf_nodes(metadata, container, **kwargs)
        extra["OQ"] = EgammaObjectQualityAlg(
            container, xAOD.EgammaParameters.BADCLUSELECTRON
        )
        if metadata["dataType"].is_simulation:
            extra["RecoSF"] = ElectronEfficiencyCorrectionAlg(
                container=container,
                dataType=metadata["dataType"],
                mode=ElectronEfficiencyCorrectionAlg.Mode.Reco,
            )
        return extra


class ElectronConfigBlock(EgammaConfigBlock):
    """Config block for electrons"""

    def __init__(self):
        super().__init__(
            object_type="electron",
            default_input_container="Electrons",
            default_subconfig_type=ElectronDefinitionConfigBlock,
        )

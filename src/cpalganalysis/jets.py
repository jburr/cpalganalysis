"""Algorithms for working with jets"""

import enum
import logging
from operator import attrgetter, itemgetter
import re
from typing import Any, Dict
from cpalganalysis.units import CLHEPReader

from cpalgnodes.algnode import SimpleAlgNode
from cpalganalysis.nodeconfigblock import NodeConfigBlock
from cpalgnodes.selection import ScaleFactorAlg, SelectionAlgNode
from cpalganalysis.objectconfigblock import ObjectConfigBlock, ObjectDefinitionConfigBlock
from cpalgnodes.utils import DataType

log = logging.getLogger(__name__)

# TODO:
# - Update to latest LRJ recommendations
# - Can we remove all mentions of b-tagging timestamps (here and in ftag.py)?


def default_calib_area() -> str:
    """Get the default jet calibration area to use"""
    return "rel21/Summer2019"


class JetCollection(object):
    """Read information from a jet collection name"""

    class InputType(enum.Enum):
        EMTopo = enum.auto()
        EMPFlow = enum.auto()
        LCTopo = enum.auto()
        TrackCaloCluster = enum.auto()
        UFOCSSK = enum.auto()
        Track = enum.auto()

    _grooming = (r"TrimmedPtFracSmallR\d+", r"SoftDropBeta\d+Zcut\d+")
    _pattern = re.compile(
        "AntiKt(\\d+|VR\\d+Rmax\\d+Rmin\\d+)({})({})?Jets".format(
            "|".join(InputType._member_names_),
            "|".join(_grooming),
        )
    )

    def __init__(self, name):
        """Interpret a collection name"""
        if isinstance(name, JetCollection):
            # just treat this as a copy constructor
            self._name = name._name
            self._radius = name._radius
            self._input = name._input
            self._grooming = name._grooming
        else:
            bt_idx = name.find("_BTagging")
            if bt_idx != -1:
                log.warning(
                    f"JetCollection {name} contains a BTagging timestamp. Only the base collection name should be provided"
                )
                name = name[:bt_idx]

            match = self._pattern.match(name)
            if not match:
                raise ValueError(
                    "Jet collection {name} does not match expected pattern!"
                )

            self._name = name
            if match.group(1).startswith("VR"):
                self._radius = match.group(1)
            else:
                self._radius = int(match.group(1))
            self._input = getattr(JetCollection.InputType, match.group(2))
            self._grooming = match.group(3)

    @property
    def name(self):
        """The collection name"""
        return self._name

    @property
    def collection_type(self):
        """The jet collection type

        Just the name with the 'Jets' suffix removed
        """
        return self.name[:-4]

    @property
    def radius(self):
        """The radius of the jet collection"""
        return self._radius

    @property
    def is_variable_radius(self):
        """Whether this is a VR jet collection"""
        return str(self._radius).startswith("VR")

    @property
    def jet_input(self) -> InputType:
        """The jet input type"""
        return self._input

    @property
    def grooming(self):
        """The jet grooming type"""
        return self._grooming

    def __str__(self):
        return self.name


class JetGhostMuonAssociationAlg(SimpleAlgNode):
    """Algorithm to perform jet <-> muon ghost association"""

    def __init__(self, container: str, name: str = None, **kwargs):
        if name is None:
            name = f"{container}JetGhostMuonAssociationAlg"
        super().__init__(
            type="CP::JetGhostMuonAssociationAlg",
            name=name,
            container=container,
            container_property="jets",
            produces_aux={"GhostMuon"},
        )


class SmallRJetUncertaintiesAlg(SimpleAlgNode):
    """Algorithm to perform systematic variations over R=0.4 jets"""

    class JESReductionScheme(enum.Enum):
        """The type of JES reduction to use"""

        AllNP = enum.auto()
        Category = enum.auto()
        Global = enum.auto()
        Strong1 = enum.auto()
        Strong2 = enum.auto()
        Strong3 = enum.auto()

    class JERNPSet(enum.Enum):
        """The set of JER nuisance parameters to use"""

        All = enum.auto()
        Full = enum.auto()
        Simple = enum.auto()
        NoJERNPs = enum.auto()

    def __init__(
        self,
        dataType: DataType,
        container: str,
        jet_collection: JetCollection,
        name: str = None,
        reduction: JESReductionScheme = JESReductionScheme.AllNP,
        jer_np: JERNPSet = JERNPSet.All,
        precalib_dep: str = "__PRECALIBRATION__",
        calib_dep: str = "__CALIBRATION__",
        calibration_area: str = default_calib_area(),
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        dataType: DataType
            The type of data being run over
        container: str
            The nickname of the container being run over
        jet_collection: JetCollection
            The type of jet collection being run over
        name: str
            The name of the algorithm
        reduction: JESReductionScheme
            The JES reduction scheme to use
        jer_np: JERNPSet
            The set of JET NPs to use
        precalib_dep: str
            Dummy dependency to make this run after jet calibration
        calib_dep: str
            Dummy dependency to make algorithms that depend on calibration run after this
        calibration_area: str
            The calibration area to read from
        """
        if jet_collection.radius != 4:
            raise ValueError(
                f"Invalid jet collection '{jet_collection}' - radius should be 0.4"
            )
        if name is None:
            name = f"{container}CalibrationUncertaintiesAlg"
        self._precalib_dep = precalib_dep
        self._calib_dep = calib_dep
        super().__init__(
            type="CP::JetUncertaintiesAlg",
            name=name,
            container_property="jets",
            container=container,
            container_out_property="jetsOut",
            requires_aux={attrgetter("_precalib_dep")},
            produces_aux={attrgetter("_calib_dep")},
            **kwargs,
        )
        tool = self.setPrivateTool(
            "uncertaintiesTool",
            "JetUncertaintiesTool",
            JetDefinition=jet_collection.collection_type,
            IsData=dataType.is_data,
            MCType="AFII" if dataType == DataType.AFII else "MC16",
        )

        # Prebuild the error message if it's a bad combination
        err = f"Invalid combination of JES reduction scheme '{reduction.name}' and JER NP set '{jer_np.name}'"
        # Check the validity of the supplied reduction and JERNP and get the right config file
        if reduction == self.JESReductionScheme.AllNP:
            if jer_np != self.JERNPSet.All:
                raise ValueError(err)
            config_file = "R4_AllNuisanceParameters_AllJERNP.config"
        elif reduction in (
            self.JESReductionScheme.Category,
            self.JESReductionScheme.Global,
        ):
            if jer_np not in (
                self.JERNPSet.Full,
                self.JERNPSet.Simple,
            ):
                raise ValueError(err)
            config_file = f"R4_{reduction.name}Reduction_{jer_np.name}JER.config"
        elif reduction in (
            self.JESReductionScheme.Strong1,
            self.JESReductionScheme.Strong2,
            self.JESReductionScheme.Strong3,
        ):
            scenario = int(reduction.name[-1])
            if jer_np == self.JERNPSet.Simple:
                config_file = f"R4_SR_Scenario{scenario}_SimpleJER.config"
            elif jer_np == self.JERNPSet.NoJERNPs:
                config_file = f"R4_SR_Scenario{scenario}.config"
            else:
                raise ValueError(err)
        tool["ConfigFile"] = f"{calibration_area}/{config_file}"


class LargeRJetUncertaintiesAlg(SimpleAlgNode):
    """Algorithm to perform systematic variations over R=1.0 jets"""

    class JESReductionScheme(enum.Enum):
        """The type of JES reduction to use"""

        AllNP = enum.auto()
        Category = enum.auto()
        Global = enum.auto()

    def __init__(
        self,
        dataType: DataType,
        container: str,
        jet_collection: JetCollection,
        name: str = None,
        reduction: JESReductionScheme = JESReductionScheme.AllNP,
        precalib_dep: str = "__PRECALIBRATION__",
        calib_dep: str = "__CALIBRATION__",
        calibration_area: str = default_calib_area(),
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        dataType: DataType
            The type of data being run over
        container: str
            The nickname of the container being run over
        jet_collection: JetCollection
            The type of jet collection being run over
        name: str
            The name of the algorithm
        reduction: JESReductionScheme
            The JES reduction scheme to use
        precalib_dep: str
            Dummy dependency to make this run after jet calibration
        calib_dep: str
            Dummy dependency to make algorithms that depend on calibration run after this
        calibration_area: str
            The calibration area to read from
        """
        if jet_collection.radius != 10:
            raise ValueError(
                f"Invalid jet collection '{jet_collection}' - radius should be 1.0"
            )
        if name is None:
            name = f"{container}CalibrationUncertaintiesAlg"
        self._precalib_dep = precalib_dep
        self._calib_dep = calib_dep
        super().__init__(
            type="CP::JetUncertaintiesAlg",
            name=name,
            container_property="jets",
            container=container,
            container_out_property="jetsOut",
            requires_aux={attrgetter("_precalib_dep")},
            produces_aux={attrgetter("_calib_dep")},
            **kwargs,
        )
        tool = self.setPrivateTool(
            "uncertaintiesTool",
            "JetUncertaintiesTool",
            JetDefinition=jet_collection.collection_type,
            IsData=dataType.is_data,
            MCType="AFII" if dataType == DataType.AFII else "MC16",
        )
        if dataType == DataType.AFII:
            raise ValueError("Large-R jet uncertainties are not supported for AFII")
        if jet_collection.collection_type == "AntiKt10LCTopoTrimmedPtFrac5SmallR20":
            if reduction == self.JESReductionScheme.AllNP:
                config_file = "R10_AllNuisanceParameters.config"
            elif reduction in (
                self.JESReductionScheme.Category,
                self.JESReductionScheme.Global,
            ):
                config_file = f"R10_{reduction.name}Reduction.config"
            else:
                raise ValueError(
                    f"Invalid JES reduction scheme '{reduction.name}' for {jet_collection.collection_type}"
                )
        else:
            raise ValueError(
                f"Unsupported large-R jet collection '{jet_collection.collection_type}'"
            )

        tool["ConfigFile"] = f"{calibration_area}/{config_file}"


class SmallRJetCalibrationAlg(SimpleAlgNode):
    """Algorithm to calibrate R=0.4 jets"""

    def __init__(
        self,
        dataType: DataType,
        container: str,
        jet_collection: JetCollection,
        name: str = None,
        use_jms: bool = False,
        precalib_dep="__PRECALIBRATION__",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        dataType: DataType
            The type of data being run over
        container: str
            The nickname of the container being run over
        jet_collection: JetCollection
            The type of jet collection being run over
        name: str
            The name of the algorithm
        use_jms: bool
            Whether to apply the RMS
        **kwargs
            Other kwargs are forwarded to the superclass
        """

        if jet_collection.radius != 4:
            raise ValueError(
                f"Invalid jet collection '{jet_collection}' - radius should be 0.4"
            )
        if name is None:
            name = f"{container}CalibrationAlg"
        self._precalib_dep = precalib_dep
        super().__init__(
            name=name,
            type="CP::JetCalibrationAlg",
            container=container,
            container_property="jets",
            container_out_property="jetsOut",
            produces_aux={attrgetter("_precalib_dep")},
            preselection_property=None,
            **kwargs,
        )
        tool = self.setPrivateTool(
            "calibrationTool",
            "JetCalibrationTool",
            JetCollection=jet_collection.collection_type,
            IsData=dataType.is_data,
        )
        # Small R configuration
        if dataType == DataType.AFII:
            config_file = "JES_MC16Recommendation_AFII_{input}_Apr2019_Rel21.config"
            if use_jms:
                raise ValueError("Small R JMS is not supported on AFII")
        else:
            if use_jms:
                if dataType == DataType.FullSim:
                    config_file = "JES_JMS_MC16Recommendation_Consolidated_MC_only_{input}_July2019_Rel21.config"
                else:
                    config_file = "JES_JMS_MC16Recommendation_Consolidated_data_only_{input}_July2019_Rel21.config"
            else:
                config_file = (
                    "JES_MC16Recommendation_Consolidated_{input}_Apr2019_Rel21.config"
                )
        if jet_collection.jet_input == JetCollection.InputType.EMPFlow:
            tool["ConfigFile"] = config_file.format(input="PFlow")
        elif jet_collection.jet_input == JetCollection.InputType.EMTopo:
            tool["ConfigFile"] = config_file.format(input="EMTopo")
        else:
            raise ValueError(
                f"Unsupported R=0.4 jet input '{jet_collection.jet_input}'"
            )
        calib_seq = "JetArea_Residual_EtaJES_GSC_{}"
        if use_jms:
            tool["CalibSequence"] = calib_seq.format(
                "JMS_Insitu" if dataType.is_data else "Smear_JMS"
            )
        else:
            tool["CalibSequence"] = calib_seq.format(
                "Insitu" if dataType.is_data else "Smear"
            )

    class Config(NodeConfigBlock):
        """Calibration block for R=0.4 jet calibration"""

        def __init__(self):
            """Create the configuration block"""
            super().__init__()
            self.add_option(
                "UseJMS",
                default=False,
                type=bool,
                help="Whether to apply JMS",
            )
            self.add_enum_option(
                "JESReduction",
                SmallRJetUncertaintiesAlg.JESReductionScheme,
                help="The JES reduction scheme to use",
            )
            self.add_enum_option(
                "JERNPSet",
                SmallRJetUncertaintiesAlg.JERNPSet,
                help="The set of JER NPs to use",
            )
            self.add_option(
                "CalibArea",
                default=default_calib_area(),
                help="The calibration area to use",
                detail=10,
                global_default="Jet/CalibArea",
            )

        def _create_nodes(
            self,
            metadata: Dict,
            container: str,
            jet_collection: JetCollection,
            precalib_dep="__PRECALIBRATION__",
            calib_dep="__CALIBRATION__",
            **kwargs,
        ):
            nodes = {
                "Calibration": SmallRJetCalibrationAlg(
                    dataType=metadata["dataType"],
                    container=container,
                    jet_collection=jet_collection,
                    use_jms=self["UseJMS"],
                    precalib_dep=(
                        precalib_dep
                        if metadata["dataType"].is_simulation
                        else calib_dep
                    ),
                )
            }
            if metadata["dataType"].is_simulation:
                nodes["Uncertainties"] = SmallRJetUncertaintiesAlg(
                    metadata["dataType"],
                    container=container,
                    jet_collection=jet_collection,
                    reduction=self["JESReduction"],
                    jer_np=self["JERNPSet"],
                    precalib_dep=precalib_dep,
                    calib_dep=calib_dep,
                )
            return nodes


class LargeRJetCalibrationAlg(SimpleAlgNode):
    """Algorithm to calibrate R=1.0 jets"""

    class MassCalibType(enum.Enum):
        Combined = enum.auto()
        Calo = enum.auto()
        TrackAssisted = enum.auto()

    def __init__(
        self,
        dataType: DataType,
        container: str,
        jet_collection: JetCollection,
        name: str = None,
        mass_type: MassCalibType = MassCalibType.Combined,
        precalib_dep="__PRECALIBRATION__",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        dataType: DataType
            The type of data being run over
        container: str
            The nickname of the container being run over
        jet_collection: JetCollection
            The type of jet collection being run over
        name: str
            The name of the algorithm
        mass_type: MassCalibType
            The type of mass calibration to use
        **kwargs
            Other kwargs are forwarded to the superclass
        """

        if jet_collection.radius != 10:
            raise ValueError(
                f"Invalid jet collection '{jet_collection}' - radius should be 1.0"
            )
        if name is None:
            name = f"{container}CalibrationAlg"
        self._precalib_dep = precalib_dep
        super().__init__(
            name=name,
            type="CP::JetCalibrationAlg",
            container=container,
            container_property="jets",
            container_out_property="jetsOut",
            produces_aux={attrgetter("_precalib_dep")},
            preselection_property=None,
            **kwargs,
        )
        tool = self.setPrivateTool(
            "calibrationTool",
            "JetCalibrationTool",
            JetCollection=jet_collection.collection_type,
            IsData=dataType.is_data,
        )
        if dataType == DataType.AFII:
            raise ValueError("Large R jet calibration is not supported in AFII")
        if jet_collection.collection_type == "AntiKt10LCTopoTrimmedPtFrac5SmallR20":
            config_file = (
                "JES_MC16recommendation_FatJet_Trimmed_JMS_{mass}_{date}.config"
            )
            calib_seq = "EtaJES_JMS"
            if dataType.is_data:
                mass = "comb"
                date = "3April2019"
                calib_seq += "_Insitu_InsituCombinedMass"
            else:
                if mass_type == self.MassCalibType.Combined:
                    mass = "comb"
                    date = "17Oct2018"
                else:
                    date = "12Oct2018"
                    if mass_type == self.MassCalibType.Calo:
                        mass = "comb"
                    else:
                        mass = "TA"
            tool["ConfigFile"] = config_file.format(mass=mass, date=date)
            tool["CalibSequence"] = calib_seq
        else:
            raise ValueError(
                f"Unsupported large R jet collection type '{jet_collection.collection_type}'"
            )

    class Config(NodeConfigBlock):
        """Calibration block for R=1.0 jet calibration"""

        def __init__(self):
            """Create the configuration block"""
            super().__init__()
            self.add_enum_option(
                "MassCalibType",
                LargeRJetCalibrationAlg.MassCalibType,
                default=LargeRJetCalibrationAlg.MassCalibType.Combined,
                help="The mass type to use in the large-R jet calibration",
            )
            self.add_enum_option(
                "JESReduction",
                LargeRJetUncertaintiesAlg.JESReductionScheme,
                help="The JES reduction scheme to use",
            )

        def _create_nodes(
            self,
            metadata: Dict,
            container: str,
            jet_collection: JetCollection,
            precalib_dep="__PRECALIBRATION__",
            calib_dep="__CALIBRATION__",
            **kwargs,
        ):
            nodes = {
                "Calibration": LargeRJetCalibrationAlg(
                    dataType=metadata["dataType"],
                    container=container,
                    jet_collection=jet_collection,
                    mass_type=self["MassCalibType"],
                    precalib_dep=precalib_dep,
                )
            }
            if metadata["dataType"].is_simulation:
                nodes["Uncertainties"] = LargeRJetUncertaintiesAlg(
                    metadata["dataType"],
                    container=container,
                    jet_collection=jet_collection,
                    reduction=self["JESReduction"],
                    precalib_dep=precalib_dep,
                    calib_dep=calib_dep,
                )
            return nodes


class JvtUpdateAlg(SimpleAlgNode):
    """Algorithm to update the JVT calculation after calibration"""

    def __init__(
        self, container: str, name: str = None, calib_dep="__CALIBRATION__", **kwargs
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the container to use
        name: str
            The name for the algorithm
        calib_dep: str
            The name of a dummy dependency to ensure that this runs after the calibration
        **kwargs
            Any extra keyword arguments are forwarded to the superclass
        """
        if name is None:
            name = f"{container}JvtUpdateAlg"
        super().__init__(
            name=name,
            type="CP::JvtUpdateAlg",
            container_property="jets",
            container=container,
            container_out_property="jetsOut",
            requires_aux={calib_dep},
            produces_aux={itemgetter("jvtTool.JVTName"), itemgetter("jvtTool.RpTName")},
            **kwargs,
        )
        self.setPrivateTool(
            "jvtTool",
            "JetVertexTaggerTool",
            JVTName="Jvt",
            RpTName="JvtRpt",
            # TODO: I'm not sure what to pass here - it seems that the NOSYS option is the best but
            # we have no way to react to the user changing this string this way
            JetContainer=container.replace("%SYS%", "NOSYS"),
        )


class JvtSelectionAlg(SelectionAlgNode, ScaleFactorAlg):
    """Algorithm to select jets based on JVT"""

    class WorkingPoint(enum.Enum):
        """The JVT working point to use"""

        Default = enum.auto()
        Loose = enum.auto()
        Medium = enum.auto()
        Tight = enum.auto()
        NoJVT = enum.auto()

    def __init__(
        self,
        container: str,
        wp: WorkingPoint,
        jet_collection: JetCollection,
        dataType: DataType,
        name: str = None,
        max_pt: float = None,
        truth_jets: str = "AntiKt4TruthDressedWZJets",
        calib_dep="__CALIBRATION__",
        jvt="Jvt",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the container to select on
        wp: WorkingPoint
            The working point to use
        jet_collection: JetCollection
            The jet collection being worked on
        dataType: DataType
            The type of data being run over
        name: str
            The name of the algorithm
        max_pt: float
            The maximum pT up to which to apply JVT
        truth_jets: str
            The truth jet collection to use for scale factor calculations
        calib_dep: str
            Dummy dependency to force this to run after calibrations
        jvt: str
            The name of the JVT decoration
        **kwargs
            Extra kwargs are passed to the superclasses
        """
        if name is None:
            name = f"{container}{wp.name}JvtSelectionAlg"
        self._calib_dep = calib_dep
        super().__init__(
            type="CP::JvtEfficiencyAlg",
            name=name,
            container=container,
            container_property="jets",
            selection_name="{}Jvt".format(wp.name),
            sf_name="{}JvtSF".format(wp.name),
            selection_property="selection",
            requires_aux={
                itemgetter("efficiencyTool.JetJvtMomentName"),
                attrgetter("_calib_dep"),
            },
            provides_inefficiency=True,
            **kwargs,
        )
        tool = self.setPrivateTool(
            "efficiencyTool",
            "CP::JetJvtEfficiency",
            WorkingPoint=(
                "None" if wp is JvtSelectionAlg.WorkingPoint.NoJVT else wp.name
            ),
            JetJvtMomentName=jvt,
        )
        if jet_collection.jet_input == JetCollection.InputType.EMPFlow:
            tool["SFFile"] = "JetJvtEfficiency/Moriond2018/JvtSFFile_EMPFlow.root"
            if max_pt is None:
                max_pt = 60e3
        elif jet_collection.jet_input == JetCollection.InputType.EMTopo:
            tool["SFFile"] = "JetJvtEfficiency/Moriond2018/JvtSFFile_EMTopoJets.root"
            if max_pt is None:
                max_pt = 120e3
        else:
            raise ValueError(
                f"Unsupported jet input for JVT {jet_collection.jet_input}"
            )
        tool["MaxPtForJvt"] = max_pt
        # Overwrite the SF name to '' if this is data
        if dataType.is_data:
            self["scaleFactorDecoration"] = ""
            self["truthJetCollection"] = ""
            self._isMC = False
        else:
            self["truthJetCollection"] = truth_jets
            self._isMC = True
        self["skipBadEfficiency"] = False

    @property
    def requires_aux(self):
        # Add the dependency on the truth jets
        requires = super().requires_aux
        if self._isMC:
            requires[self["truthJetCollection"]] = set()
        return requires

    class Config(NodeConfigBlock):
        """Configuration block for JVT selection and scale factors"""

        def __init__(self):
            """Create the block"""
            super().__init__()
            self.add_enum_option(
                "WP",
                JvtSelectionAlg.WorkingPoint,
                help="The JVT working point to use",
                set_from_single_value=True,
            )
            self.add_option(
                "MaxPt",
                default=None,
                type=CLHEPReader("GeV"),
                required=False,
                help="The maximum pT up to which to apply JVT",
            )

        def _create_nodes(
            self,
            metadata: Dict,
            container: str,
            jet_collection: JetCollection,
            calib_dep: str = "__CALIBRATION__",
            jvt: str = "Jvt",
            truth_jets: str = "AntiKt4TruthDressedWZJets",
            **kwargs,
        ):
            return JvtSelectionAlg(
                container=container,
                wp=self["WP"],
                jet_collection=jet_collection,
                dataType=metadata["dataType"],
                max_pt=self["MaxPt"].clhep_value if self["MaxPt"] else None,
                truth_jets=truth_jets,
                calib_dep=calib_dep,
                jvt=jvt,
            )


# TODO: fJVT


class SmallRJetDefinitionConfigBlock(ObjectDefinitionConfigBlock):
    """Configuration block for jet selection"""

    def __init__(self):
        """Create the block"""
        from cpalganalysis.ftag import FTagSelectionAlg

        super().__init__(
            use_cluster_eta=False,
            add_crack_veto_opt=False,
            order=["Kinematic", "Jvt", "FTag"],
        )
        self.add_subconfig("Jvt", JvtSelectionAlg.Config())
        self.add_subconfig("FTag", FTagSelectionAlg.Config())


class SmallRJetConfigBlock(ObjectConfigBlock):
    """Config block for jets"""

    def __init__(self):
        super().__init__(
            object_type="jet",
            default_subconfig_type=SmallRJetDefinitionConfigBlock,
            default_input_container=lambda c: c["JetCollection"].name,
        )
        self.add_option(
            "JetCollection",
            type=JetCollection,
            help="The type of small-R jet collection to run over",
            validator=lambda jet_collection: jet_collection.radius == 4,
        )
        self.add_subconfig("Calibration", SmallRJetCalibrationAlg.Config(), always=True)

    def _create_nodes_kwargs(self, metadata: Dict[str, Any], **kwargs) -> Dict:
        return super()._create_nodes_kwargs(metadata, **kwargs) | {
            "jet_collection": self["JetCollection"]
        }

    def _create_nodes(
        self,
        metadata: Dict,
        container: str,
        calib_dep: str,
        **kwargs,
    ):

        nodes = super()._create_nodes(
            metadata, container=container, calib_dep=calib_dep, **kwargs
        )
        nodes["UpdateJvt"] = JvtUpdateAlg(container, calib_dep=calib_dep)
        return nodes


class LargeRJetDefinitionConfigBlock(ObjectDefinitionConfigBlock):
    """Configuration block for large-R jet selection

    Right now this only contains kinematic selections, but in the future we could include boosted
    object tagging here.
    """

    def __init__(self):
        """Create the block"""
        super().__init__(
            use_cluster_eta=False,
            add_crack_veto_opt=False,
            order=["Kinematic"],
        )


class LargeRJetConfigBlock(ObjectConfigBlock):
    """Configuration block for large-R jets"""

    def __init__(self):
        super().__init__(
            object_type="lrjet",
            default_subconfig_type=LargeRJetDefinitionConfigBlock,
            default_input_container=lambda c: c["JetCollection"].name,
        )
        self.add_option(
            "JetCollection",
            type=JetCollection,
            help="The type of large-R jet collection to run over",
            validator=lambda jet_collection: jet_collection.radius == 10,
        )
        self.add_subconfig(
            "Calilbration", LargeRJetCalibrationAlg.Config(), always=True
        )

    def _create_nodes_kwargs(self, metadata: Dict[str, Any], **kwargs) -> Dict:
        return super()._create_nodes_kwargs(metadata, **kwargs) | {
            "jet_collection": self["JetCollection"]
        }

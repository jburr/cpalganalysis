"""Config block base class for configurations that produce nodes"""

from collections import defaultdict
import dataclasses
import logging
from typing import Any, Dict, List, Optional, Set, Union

from configblock.configblock import ConfigBlock
from cpalgnodes.node import Node

log = logging.getLogger(__name__)

NodeDict = Dict[str, Union[Node, "NodeDict"]]


@dataclasses.dataclass
class ContainerIOInfo:
    """Simple class to contain input and output data for a container"""

    #: The nickname of the container
    nickname: str
    #: The name of the input container in the StoreGate
    input: Optional[str] = None
    # The name of the output container in the StoreGate
    output: Optional[str] = None


class NodeConfigBlock(ConfigBlock):

    """---Methods to create the nodes---"""

    def create_nodes(self, metadata: Dict, validate=True, **kwargs) -> NodeDict:
        """Create the nodes described by this configuration

        Parameters
        ----------
        metadata : Dict
            Metadata read from the input file
        validate :bool
            Whether to validate the configuration first, by default True
        **kwargs
            Extra keyword arguments to be passed to the rest of the create methods
        """
        if validate:
            self.validate()
        return self._create_nodes(
            metadata, **self._create_nodes_kwargs(metadata, **kwargs)
        )

    def _create_nodes_kwargs(self, metadata: Dict[str, Any], **kwargs) -> Dict:
        """Return extra keyword args to be passed to the _create_nodes method

        Parameters
        ----------
        metadata: Dict[str, Any]
            The metadata from the input file
        **kwargs:
            The kwargs passed to our _create_nodes function
        """
        return kwargs

    def _create_nodes(self, metadata: Dict, **kwargs):
        """Do the actual work to create the nodes"""
        log.debug("Create nodes for %s", self.full_path)
        return {
            str(k): cfg.create_nodes(metadata, validate=False, **kwargs)
            for k, cfg in self.iter_subconfigs(nested=False, only_active=True)
        }

    """---Methods giving the input and output containers used/provided by this configuration---"""

    def defined_containers(self, nested: bool = True) -> List[ContainerIOInfo]:
        """Return information on the containers defined in this config

        Parameters
        ----------
        nested : bool, optional
            If true, recurse through all subconfigs and collate the result, by default True

        Returns
        -------
        List[ContainerInfo]
            All defined containers
        """
        containers = self._defined_containers()
        if nested:
            for _, cfg in self.iter_subconfigs(nested=False, only_active=True):
                # Not all subconfigs (e.g. global) will be NodeConfigBlocks
                try:
                    containers += cfg.defined_containers(nested=True)
                except AttributeError:
                    pass
        return containers

    def _defined_containers(self) -> List[ContainerIOInfo]:
        """The containers defined directly on this config

        Should be overridden in derived classes

        Returns
        -------
        List[ContainerIOInfo]
            All containers defined directly by this configuration
        """
        return []

    @property
    def io_dictionaries(self) -> Dict[str, Dict[str, str]]:
        """The inputs and outputs required by this config

        Should be used as kwargs to the scheduler create_schedule method

        Returns
        -------
        Dict[str, Dict[str, str]]
            A dictionary containing two sub-dictionaries. The 'inputs' key contains a dictionary
            mapping from container nickname to input name, the 'outputs' key contains a dictionary
            mapping from container to output name.

        Raises
        ------
        ValueError
            Two separate configs have conflicting information for input/output information
        """
        inputs = {}
        outputs = {}
        for info in self.defined_containers(nested=True):
            if info.input is not None:
                try:
                    existing = inputs[info.nickname]
                except KeyError:
                    inputs[info.nickname] = info.input
                else:
                    if existing != info.input:
                        raise ValueError(
                            "Conflicting input names for container '{}' ('{}' != '{}')".format(
                                info.nickname, existing, info.input
                            )
                        )
            if info.output is not None:
                try:
                    existing = outputs[info.nickname]
                except KeyError:
                    outputs[info.nickname] = info.output
                else:
                    if existing != info.output:
                        raise ValueError(
                            "Conflicting output names for container '{}' ('{}' != '{}')".format(
                                info.nickname, existing, info.output
                            )
                        )
        return {"inputs": inputs, "outputs": outputs}

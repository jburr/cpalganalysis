import enum
from operator import attrgetter
from typing import Dict
from cpalganalysis.nodeconfigblock import NodeConfigBlock
from cpalganalysis.egamma import (
    EgammaConfigBlock,
    EgammaIsolationSelectionAlg,
    EgammaObjectQualityAlg,
    EgammaIsolationCorrectionAlg,
)
from cpalgnodes.selection import SelectionAlgNode, ScaleFactorAlg
from cpalganalysis.objectconfigblock import ObjectDefinitionConfigBlock
from cpalgnodes.utils import DataType

from xAODEgamma.xAODEgammaParameters import xAOD

class PhotonEfficiencyCorrectionAlg(ScaleFactorAlg):
    """Provide scale factors for photon selections"""

    class Mode(enum.Enum):
        Reco = enum.auto()
        Iso = enum.auto()

    def __init__(
        self,
        container: str,
        dataType: DataType,
        mode: Mode,
        wp: str = None,
        name: str = None,
        sf_name: str = None,
        calib_dep: str = "__CALIBRATION__",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the container to select on
        dataType: DataType
            The datatype being used
        mode: Mode
            The type of scale factor to calculate (Reco or Iso)
        wp: str
            The working point to use (in Iso mode)
        name: str
            The name of the algorithm
        sf_name: str
            The name of the scale factor to create. Can be left as None in which case a sensible
            default is chosen
        calib_dep: str
            Dummy dependency to allow this to depend on the calibration
        **kwargs
            Any extra kwargs are passed to the superclass
        """
        if mode == PhotonEfficiencyCorrectionAlg.Mode.Reco:
            wp = ""
        elif wp is None:
            raise ValueError("A working point must be provided in isolation mode")
        if name is None:
            name = f"{container}{wp}{mode.name}EfficiencyCorrectionAlg"
        if sf_name is None:
            sf_name = f"{wp}{mode.name}SF_%SYS%"
        self._calib_dep = calib_dep
        super().__init__(
            type="CP::PhotonEfficiencyCorrectionAlg",
            name=name,
            container_property="photons",
            container=container,
            sf_name=sf_name,
            requires_aux={attrgetter("_calib_dep")},
            **kwargs,
        )
        tool = self.setPrivateTool(
            "efficiencyCorrectionTool",
            "AsgPhotonEfficiencyCorrectionTool",
            MapFilePath="PhotonEfficiencyCorrection/2015/2017/rel21.2/Winter2018_Prerec_v1/map0.txt",
            ForceDataType=dataType.value,
        )
        if mode == PhotonEfficiencyCorrectionAlg.Mode.Iso:
            tool["IsoKey"] = wp


class PhotonIsolationSelectionAlg(EgammaIsolationSelectionAlg):
    """Algorithm to select photons based on isolation"""

    class WorkingPoint(enum.Enum):
        TightCaloOnly = enum.auto()
        FixedCutTight = enum.auto()
        FixedCutLoose = enum.auto()
        Tight = enum.auto()
        Loose = enum.auto()

    def __init__(
        self,
        container: str,
        wp: WorkingPoint,
        name: str = None,
        calib_dep: str = "__CALIBRATION__",
        isocorr_dep: str = "__ISOCORR__",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the container to select on
        wp: WorkingPoint
            The working point to use
        name: str
            The name of the algorithm
        calib_dep: str
            Dummy calibration dependency to force this to run after calibration
        isocorr_dep: str
            Dummy correction dependency to force this to run after the isolation correction
        **kwargs
            Extra kwargs are passed to the superclass
        """
        super().__init__(
            container=container,
            wp=wp.name,
            tool_prop="PhotonWP",
            name=name,
            calib_dep=calib_dep,
            isocorr_dep=isocorr_dep,
            **kwargs,
        )

    class Config(NodeConfigBlock):
        """Config block for electron isolation selection"""

        def __init__(self):
            """Create the configuration"""
            super().__init__()
            self.add_enum_option(
                "Iso",
                PhotonIsolationSelectionAlg.WorkingPoint,
                help="The isolation working point to use",
                set_from_single_value=True,
            )

        def _create_nodes(
            self,
            metadata: Dict,
            container: str,
            calib_dep: str,
            isocorr_dep: str = "__ISOCORR__",
            **kwargs,
        ):
            nodes = {
                "Sel": PhotonIsolationSelectionAlg(
                    container=container,
                    wp=self["Iso"],
                    calib_dep=calib_dep,
                    isocorr_dep=isocorr_dep,
                )
            }
            if metadata["dataType"].is_simulation:
                nodes["IsolationCorrection"] = EgammaIsolationCorrectionAlg(
                    container=container,
                    dataType=metadata["dataType"],
                    isocorr_dep=isocorr_dep,
                )
                nodes["SF"] = PhotonEfficiencyCorrectionAlg(
                    container=container,
                    dataType=metadata["dataType"],
                    mode=PhotonEfficiencyCorrectionAlg.Mode.Iso,
                    wp=self["Iso"],
                )
            return nodes


class PhotonIsEMSelectionAlg(SelectionAlgNode):
    """Photon ID selection"""

    # TODO: Should we include the other working points here?
    class WorkingPoint(enum.Enum):
        Loose = enum.auto()
        Tight = enum.auto()

    def __init__(
        self,
        container: str,
        wp: WorkingPoint,
        recompute_isem: bool = False,
        name: str = None,
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the input container
        wp: WorkingPoint
            The name of the working point to use
        recompute_isem: bool
            If True, recalculate the is EM value, otherwise use the DF-level decorations
        name: str
            The name of the algorithm
        **kwargs
            Extra kwargs are forwarded to the superclass
        """
        if name is None:
            name = f"{container}{wp}IDIsEMSelectionAlg"
        super().__init__(
            name=name, container=container, selection_name=f"{wp.name}ID", **kwargs
        )
        if recompute_isem:
            # TODO Should we use WorkingPoint or set ConfigFiel and isEMMask

            self.setPrivateTool(
                "selectionTool",
                "AsgPhotonIsEMSelector",
                WorkingPoint=f"{wp.name}Photon",
                # isEMMask=getattr(ROOT.egammaPID, "Photon" + wp),
                # ConfigFile="ElectronPhotonSelectorTools/offline/20180116/PhotonIsEMTightSelectorCutDefs.conf",
            )
            self.selection_count = 32
        else:
            self.setPrivateTool(
                "selectionTool",
                "CP::AsgFlagSelectionTool",
                selectionFlags=["DFCommonPhotonsIsEM" + wp.name],
            )
            self.selection_count = 1

    class Config(NodeConfigBlock):
        def __init__(self):
            super().__init__()
            self.add_enum_option(
                "ID",
                PhotonIsEMSelectionAlg.WorkingPoint,
                help="The photon ID working point to use",
                set_from_single_value=True,
            )
            self.add_option(
                "RecomputeIsEM",
                False,
                type=bool,
                help="Whether to calculate the likelihood or use the derivation flags",
            )

        def _create_nodes(self, metadata: Dict, container: str, **kwargs):
            nodes = {
                "Sel": PhotonIsEMSelectionAlg(
                    container,
                    wp=self["ID"],
                    recompute_isem=self["RecomputeIsEM"],
                )
            }
            # if metadata["dataType"].is_simulation:
                # TODO: Showershape/variable correction
            return nodes


class PhotonDefinitionConfigBlock(ObjectDefinitionConfigBlock):
    """Config block for a single photon selection"""

    def __init__(self):
        """Create the config block"""
        super().__init__(
            use_cluster_eta=True,
            add_crack_veto_opt=True,
            order=["OQ", "Eta", "ID", "Pt", "Iso"],
        )

        self.add_option(
            "GoodOQ", type=bool, default=True, detail=10, help="Apply the quality cut"
        )
        self.add_subconfig("ID", PhotonIsEMSelectionAlg.Config())
        self.add_subconfig("Iso", PhotonIsolationSelectionAlg.Config())

    def _create_selection_and_sf_nodes(self, metadata: Dict, container: str, **kwargs):
        extra = super()._create_selection_and_sf_nodes(
            metadata, container=container, **kwargs
        )
        extra["OQ"] = EgammaObjectQualityAlg(
            container, xAOD.EgammaParameters.BADCLUSPHOTON
        )
        if metadata["dataType"].is_simulation:
            extra["RecoSF"] = PhotonEfficiencyCorrectionAlg(
                    container=container,
                    dataType=metadata["dataType"],
                    mode=PhotonEfficiencyCorrectionAlg.Mode.Reco,
            )
        return extra


class PhotonConfigBlock(EgammaConfigBlock):
    """Config block for photons"""

    def __init__(self):
        super().__init__(
            object_type="photon",
            default_input_container="Photons",
            default_subconfig_type=PhotonDefinitionConfigBlock,
        )

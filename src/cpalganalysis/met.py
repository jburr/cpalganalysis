"""Algorithm nodes for MET calculations"""

import copy
from collections import defaultdict
import enum
import logging
from operator import itemgetter
import re
from typing import Any, Dict, List, Set, Tuple

import boolean
from cpalganalysis.nodeconfigblock import ContainerIOInfo, NodeConfigBlock
from cpalganalysis.utils import find_output_container_from_subconfig_type
from configblock.utils import find_option_on_block_of_type, mk_optional
from cpalgnodes.algnode import AlgNode, SimpleAlgNode
from cpalgnodes.booleanalgebra import algebra
from cpalgnodes.componentfacade import Algorithm
from cpalgnodes.node import Node
from cpalgnodes.utils import DataType, add_sys_pattern, replace_sys_pattern

from cpalganalysis.jets import JetCollection

log = logging.getLogger(__name__)

# TODO:
# - Change the met maker alg so that it takes preselection properties so we don't have to create view containers on the fly
# - Allow setting the jet selection, etc
# - Should we update the MET significance tool to have a configurable fJVT decoration?
# - Should we support configurable term names or choosing the soft term?
# - Properly document the different soft term parameterisations
# - Should we put something in to signify that preselections make no sense for MET?


class MetMakerAlg(AlgNode):
    """Algorithm to calculate MET"""

    def __init__(
        self,
        dataType: DataType,
        container: str,
        jet_collection: JetCollection,
        jets: str,
        electrons: str = None,
        photons: str = None,
        muons: str = None,
        taus: str = None,
        invisible: str = None,
        name: str = None,
        fjvt: str = None,
        electron_presel: str = None,
        photon_presel: str = None,
        muon_presel: str = None,
        tau_presel: str = None,
        jet_calib_dep: str = "__CALIBRATION__",
        electron_calib_dep: str = "__CALIBRATION__",
        photon_calib_dep: str = "__CALIBRATION__",
        muon_calib_dep: str = "__CALIBRATION__",
        tau_calib_dep: str = "__CALIBRATION__",
        **kwargs,
    ):
        if name is None:
            name = f"{container}MetMakerAlg"
        super().__init__(
            type="CP::MetMakerAlg",
            name=name,
            metCore=f"MET_Core_{jet_collection.collection_type}",
            metAssociation=f"METAssoc_{jet_collection.collection_type}",
            **kwargs,
        )
        self._extra_deps = defaultdict(set)
        self._extra_deps[jets].add(jet_calib_dep)
        self.add_input("jets", jets)
        self.add_output("met", container)
        self._preselections = {}

        maker = self.setPrivateTool(
            "makerTool",
            "met::METMaker",
            DoPFlow=jet_collection.jet_input == JetCollection.InputType.EMPFlow,
        )
        if fjvt is not None:
            maker["JetRejectionDec"] = fjvt

        if dataType.is_simulation:
            self.setPrivateTool("systematicsTool", "met::METSystematicsTool")

        if electrons is not None:
            self.add_input("electrons", electrons)
            self._extra_deps[electrons].add(electron_calib_dep)
            if electron_presel is not None:
                self._preselections[electrons] = electron_presel

        if photons is not None:
            self.add_input("photons", photons)
            self._extra_deps[photons].add(photon_calib_dep)
            if photon_presel is not None:
                self._preselections[photons] = photon_presel

        if muons is not None:
            self.add_input("muons", muons)
            self._extra_deps[muons].add(muon_calib_dep)
            if muon_presel is not None:
                self._preselections[muons] = muon_presel

        if taus is not None:
            self.add_input("taus", taus)
            self._extra_deps[taus].add(tau_calib_dep)
            if tau_presel is not None:
                self._preselections[taus] = tau_presel

        if invisible is not None:
            self.add_input("invisible", invisible)

    @property
    def requires_aux(self) -> Dict[str, Set[str]]:
        requires = copy.deepcopy(self._extra_deps)
        for container, presel in self._preselections.items():
            presel = algebra.parse(presel)
            requires[container].update(map(str, presel.symbols))
        try:
            fjvt = self["makerTool"]["JetRejectionDec"]
        except KeyError:
            pass
        else:
            requires[self._inputs["jets"]].add(fjvt)
        requires.update({self["metCore"]: {}, self["metAssociation"]: {}})
        return requires

    @property
    def produces_aux(self) -> Dict[str, Set[str]]:
        return {}

    @property
    def produces_containers(self) -> Dict[str, str]:
        return {self._outputs["met"]: None}

    def requires_objects(
        self, required_output: Dict[Tuple[str, str], boolean.Expression]
    ) -> Dict[Tuple[str, str], boolean.Expression]:
        requires = {}
        for container, aux_items in self.requires_aux.items():
            # As we never set a preselection on the jets this will always request all the jets
            try:
                presel = algebra.parse(self._preselections[container])
            except KeyError:
                presel = Node.ALL_OBJECTS
            for aux in aux_items:
                requires[(container, aux)] = presel
        return requires

    def create(self, container_info):
        main_alg = super().create(container_info)
        presel_algs = []
        # Now we have to make the preselections
        for prop, container in self._inputs.items():
            try:
                presel = self._preselections[container]
            except KeyError:
                continue
            info = container_info[container]
            presel = info.selection_to_property_value(presel)
            presel_alg = Algorithm(
                type="CP::AsgViewFromSelectionAlg",
                name=f"{self.name}{container}PreselectionAlg",
                input=info.input_name,
                selection=[presel],
                output=f"{container}{self.name}Preselection",
            )
            presel_algs.append(presel_alg)
            main_alg[prop] = presel_alg["output"]
        # Also add the builder alg at the end

        builder = Algorithm(
            "CP::MetBuilderAlg",
            f"{self._outputs['met']}MetBuilderAlg",
            met=main_alg["met"],
        )
        return presel_algs + [main_alg, builder]

    class Config(NodeConfigBlock):
        """The configuration block for overlap removal"""

        # TODO: Right now we have no way of providing an invisible container to this

        def __init__(self):
            """Create the configuration block"""
            from cpalganalysis.electrons import ElectronConfigBlock
            from cpalganalysis.muons import MuonConfigBlock
            from cpalganalysis.photons import PhotonConfigBlock
            from cpalganalysis.taus import TauConfigBlock
            from cpalganalysis.jets import SmallRJetConfigBlock

            super().__init__()
            self.add_option(
                "JetCollection",
                type=JetCollection,
                help="The type of jet collection being used. Should match the jets provided to 'InputJets'",
                default=find_option_on_block_of_type(
                    SmallRJetConfigBlock, "JetCollection", required=True
                ),
            )
            self.add_option(
                "InputJets",
                default=find_output_container_from_subconfig_type(
                    SmallRJetConfigBlock, required=True
                ),
                type=replace_sys_pattern,
                help="The input jet container",
            )
            self.add_option(
                "InputElectrons",
                default=find_output_container_from_subconfig_type(
                    ElectronConfigBlock, required=False
                ),
                type=mk_optional(replace_sys_pattern),
                help="The input electron container",
            )
            self.add_option(
                "InputMuons",
                default=find_output_container_from_subconfig_type(
                    MuonConfigBlock, required=False
                ),
                type=mk_optional(replace_sys_pattern),
                help="The input muon container",
            )
            self.add_option(
                "InputPhotons",
                default=find_output_container_from_subconfig_type(
                    PhotonConfigBlock, required=False
                ),
                type=mk_optional(replace_sys_pattern),
                help="The input photon container",
            )
            self.add_option(
                "InputTaus",
                default=find_output_container_from_subconfig_type(
                    TauConfigBlock, required=False
                ),
                type=mk_optional(replace_sys_pattern),
                help="The input tau jet container",
            )
            self.add_option(
                "InputSelection",
                required=False,
                help="Selection to apply to the input objects (except jets). The object specific options will override this",
            )
            self.add_option(
                "ElectronSelection",
                default="@InputSelection",
                help="Selection to apply to the input electrons",
            )
            self.add_option(
                "MuonSelection",
                default="@InputSelection",
                help="Selection to apply to the input muons",
            )
            self.add_option(
                "PhotonSelection",
                default="@InputSelection",
                help="Selection to apply to the input photons",
            )
            self.add_option(
                "TauSelection",
                default="@InputSelection",
                help="Selection to apply to the input tau jets",
            )
            self.add_option(
                "FJVT",
                required=False,
                help="The fJVT selection to apply (if any)",
            )

        def _create_nodes_kwargs(
            self, metadata: Dict[str, Any], calib_dep: str = "__CALIBRATION__", **kwargs
        ) -> Dict:
            kwargs = super()._create_nodes_kwargs(
                metadata, calib_dep=calib_dep, **kwargs
            )
            for object in "jet", "electron", "muon", "photon", "tau":
                kwargs.setdefault(f"{object}_calib_dep", calib_dep)
            return kwargs

        def _create_nodes(
            self,
            metadata: Dict,
            container: str,
            jet_calib_dep: str = "__CALIBRATION__",
            electron_calib_dep="__CALIBRATION__",
            muon_calib_dep="__CALIBRATION__",
            photon_calib_dep="__CALIBRATION__",
            tau_calib_dep="__CALIBRATION__",
            **kwargs,
        ):
            return MetMakerAlg(
                dataType=metadata["dataType"],
                container=container,
                jet_collection=self["JetCollection"],
                jets=self["InputJets"],
                electrons=self["InputElectrons"],
                muons=self["InputMuons"],
                photons=self["InputPhotons"],
                taus=self["InputTaus"],
                fjvt=self["FJVT"],
                electron_presel=self["ElectronSelection"],
                muon_presel=self["MuonSelection"],
                photon_presel=self["PhotonSelection"],
                tau_presel=self["TauSelection"],
                jet_calib_dep=jet_calib_dep,
                electron_calib_dep=electron_calib_dep,
                muon_calib_dep=muon_calib_dep,
                photon_calib_dep=photon_calib_dep,
                tau_calib_dep=tau_calib_dep,
            )


class MetSignificanceAlg(SimpleAlgNode):
    """Algorithm to calculate MET significance

    The MET significance will be decorated on the MET final term
    """

    class SoftTermParams(enum.Enum):
        """Soft term parameterisations"""

        Random = 0
        PthardParam = 1
        TSTParam = 2

    def __init__(
        self,
        dataType: DataType,
        container: str,
        treat_pu_jets: bool = False,
        soft_term_param: SoftTermParams = SoftTermParams.Random,
        name: str = None,
        decoration: str = "significance",
        **kwargs,
    ):
        if name is None:
            name = f"{container}MetSignificanceAlg"
        super().__init__(
            type="CP::MetSignificanceAlg",
            name=name,
            container_property="met",
            container=container,
            preselection_property=None,
            significanceDecoration=decoration,
            produces_aux={itemgetter("significanceDecoration")},
            **kwargs,
        )
        self.setPrivateTool(
            "significanceTool",
            "met::METSignificance",
            SoftTermParam=soft_term_param.value,
            TreatPUJets=treat_pu_jets,
            IsAFII=dataType == DataType.AFII,
        )

    class Config(NodeConfigBlock):
        """Configuration for the MET significance alg"""

        def __init__(self):
            """Create the configuration"""
            super().__init__()
            self.add_option(
                "TreatPUJets",
                default=False,
                type=bool,
                help="Add an extra uncertainty for PU jets based on JVT and fJVT",
            )
            self.add_enum_option(
                "SoftTermParameterisation",
                MetSignificanceAlg.SoftTermParams,
                default="Random",
                help="The soft term parameterisation to use",
            )

        def _create_nodes(self, metadata: Dict, container: str, **kwargs):
            return MetSignificanceAlg(
                dataType=metadata["dataType"],
                container=container,
                treat_pu_jets=self["TreatPUJets"],
                soft_term_param=self["SoftTermParameterisation"],
            )


class MetConfigBlock(NodeConfigBlock):
    """Configuration block for MET calculations"""

    def __init__(self):
        """Create the block"""
        super().__init__()
        self.add_option(
            "OutputContainer",
            type=add_sys_pattern,
            help="The name of the MET container to create. If it does not contain '%SYS%' this will be added",
            detail=10,
            set_from_key=True,
        )
        self.add_subconfig("Maker", MetMakerAlg.Config())
        self.add_subconfig("Significance", MetSignificanceAlg.Config())

    def _check_errors(self) -> bool:
        error = False
        if self["Significance/TreatPUJets"] and self["Maker/FJVT"] is None:
            log.error(
                f"{self.full_path}: MET Significance pile-up treatment requires fJVT"
            )
            error = True
        return error | super()._check_errors()

    @property
    def container(self) -> str:
        """The nickname for this container"""
        # Set the container nickname to be the output container without any %SYS% pattern
        return replace_sys_pattern(self["OutputContainer"])

    def _create_nodes_kwargs(self, metadata: Dict[str, Any], **kwargs) -> Dict:
        return super()._create_nodes_kwargs(metadata, **kwargs) | {
            "container": self.container
        }

    def _defined_containers(self) -> List[ContainerIOInfo]:
        return [ContainerIOInfo(self.container, output=self["OutputContainer"])]

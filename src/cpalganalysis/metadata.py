from AthenaConfiguration.AthConfigFlags import AthConfigFlags
from collections.abc import MutableMapping
from typing import Iterable
from cpalgnodes.utils import data_type_from_flags
from logging import log

# TODO:
# - It's not unreasonable to have a metadata item that depends on another. This could be
#   accommodated by having the callables take the dictionary itself as an argument. However this
#   requires clearing the cache every time that you modify a key which is rather annoying

# NB: Not convinced yet about the name for the MC21 we have right now
_mc_periods = {
        284500: "mc16a",
        300000: "mc16d",
        310000: "mc16e",
        330000: "mc21a",
}

class MetaData(MutableMapping):
    """Dictionary class that allows for values that depend on the values stored in configuration
    flags"""

    def __init__(self, flags: AthConfigFlags, **kwargs):
        """Create the dictionary with its underlying flags
        
        As we cache the results of calls it's not valid to 
        """
        if not flags.locked():
            raise ValueError("Cannot provide unlocked flags to a MetaData object")
        self._values = {}
        self._flags = flags
        self.update(kwargs)

    def __len__(self):
        return len(self._values)

    def __iter__(self):
        return iter(self._values)

    def __getitem__(self, key):
        """Return an item from the mapping

        If the item is a callable it will be called with the flags as an argument
        """
        value = self._values[key]
        if callable(value):
            try:
                value = value(self._flags)
            except:
                raise KeyError(key)
            # TODO: This provides no way to clear caches or something like that.
            self._values[key] = value
        return value

    def __setitem__(self, key, value):
        self._values[key] = value

    def __delitem__(self, key):
        del self._values[key]


# TODO: Surely there must be central functions for this somewhere...?
def mc_period_from_flags(flags: AthConfigFlags):
    return _mc_periods[flags.Input.RunNumber[0]]

def lhc_run_from_flags(flags: AthConfigFlags):
    run_number = flags.Input.RunNumber[0]
    if flags.Input.isMC:
        if run_number >= 330000:
            return 3
        else:
            return 2
    else:
        if run_number <= 260000:
            return 1
        elif run_number <= 365000:
            return 2
        else:
            return 3


def default_metadata(flags: AthConfigFlags):
    return MetaData(
            flags,
            dataType = data_type_from_flags,
            dsid = lambda flags: falgs.Input.MCChannelNumber,
            period = mc_period_from_flags,
            LHCRun = lhc_run_from_flags,
    )

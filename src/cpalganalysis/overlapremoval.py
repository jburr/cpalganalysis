from collections import defaultdict
import copy
from typing import Any, Dict, Iterable, Tuple

import boolean
from configblock.utils import mk_optional

from cpalgnodes.booleanalgebra import algebra
from cpalgnodes.algnode import AlgNode
from cpalganalysis.nodeconfigblock import NodeConfigBlock
from cpalgnodes.node import Node
from cpalganalysis.utils import find_output_container_from_subconfig_type
from cpalgnodes.selection import Selection
from cpalgnodes.utils import add_sys_pattern, replace_sys_pattern

# TODO: this does not do the user priority style OR used by SUSYTools yet


class OverlapRemovalAlg(AlgNode):
    """Algorithm to remove overlaps between input objects"""

    def __init__(
        self,
        name: str = None,
        input_label: str = "selected",
        output_label: str = "passOR",
        bjet_label: str = None,
        jets=None,
        electrons=None,
        muons=None,
        photons=None,
        taus=None,
        lrjets=None,
        do_ele_ele: bool = False,
        boosted_leptons: bool = False,
        do_mu_pf_jet: bool = False,
        link_overlapping_objects=False,
        jet_calib_dep="__CALIBRATION__",
        electron_calib_dep="__CALIBRATION__",
        muon_calib_dep="__CALIBRATION__",
        photon_calib_dep="__CALIBRATION__",
        tau_calib_dep="__CALIBRATION__",
        lrjet_calib_dep="__CALIBRATION__",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        name: str
            The name for the algorithm
        input_label: str
            Only consider objects passing the input label
        output_label: str
            Decorate objects passing the OR with this label
        bjet_label: str
            If using b-tag aware OR use this input label. If left as None, do nont use b-tag aware OR
        jets: str
            The name of the input jet container, if None do not use jets
        electrons: str
            The name of the input electrons container, if None do not use electrons
        muons: str
            The name of the input muon container, if None do not use muons
        photons: str
            The name of the input photon container, if None do not use photons
        taus: str
            The name of the input tau-jet container, if None do not use taus
        lrjets: str
            The name of the input large-radius jet container, if None do not use LR jets
        do_ele_ele: bool
            Remove overlaps between electrons
        boosted_leptons: bool
            Use settings for boosted leptons
        do_mu_pf_jet: bool
            Remove PFlow jets which are likely just muons
        link_overlapping_objects: bool
            Set an element link between overlapping objects
        jet_calib_dep: str
            Dummy dependency to force this to run after jet calibration
        electron_calib_dep: str
            Dummy dependency to force this to run after electron calibration
        muon_calib_dep: str
            Dummy dependency to force this to run after muon calibration
        photon_calib_dep: str
            Dummy dependency to force this to run after photon calibration
        tau_calib_dep: str
            Dummy dependency to force this to run after tau calibration
        lrjet_calib_dep: str
            Dummy dependency to force this to run after LR jet calibration
        **kwargs
            Extra keyword arguments are passed to the super class
        """
        if name is None:
            name = self.create_unique_name("OverlapRemovalAlg{index}")
        if bjet_label is None:
            bjet_label = ""
        self._input_label = input_label
        self._output_label = output_label
        self._extra_deps = defaultdict(set)
        super().__init__(
            type="CP::OverlapRemovalAlg", name=name, OutputLabel=output_label, **kwargs
        )

        # Properties we're going to have to set on all the tools
        common_props = {
            "InputLabel": input_label,
            "OutputLabel": output_label,
            "OutputPassValue": True,
        }
        for obj in ["jet", "electron", "muon", "photon", "tau", "lrjet"]:
            plural = f"{obj}s"
            container = locals()[plural]
            if container:
                self.add_input(plural, container)
                self._extra_deps[container].add(locals()[f"{obj}_calib_dep"])
                if obj == "lrjet":
                    obj = "fatJet"
                self[f"{obj}sDecoration"] = add_sys_pattern(output_label)

        tool = self.setPrivateTool(
            "overlapTool",
            "ORUtils::OverlapRemovalTool",
            **common_props,
        )
        common_props["LinkOverlapObjects"] = link_overlapping_objects

        if do_mu_pf_jet and muons and jets:
            tool.setPrivateTool(
                "MuPFJetORT",
                "ORUtils::MuPFJetOverlapTool",
                BJetLabel=bjet_label,
                **common_props,
            )

        if do_ele_ele and electrons:
            tool.setPrivateTool(
                "EleEleORT", "ORUtils::EleJetOverlapTool", **common_props
            )

        if electrons and muons:
            tool.setPrivateTool(
                "EleMuORT", "ORUtils::EleMuSharedTrkOverlapTool", **common_props
            )

        if electrons and jets:
            tool.setPrivateTool(
                "EleJetORT",
                "ORUtils::EleJetOverlapTool",
                BJetLabel=bjet_label,
                UseSlidingDR=boosted_leptons,
                **common_props,
            )

        if muons and jets:
            tool.setPrivateTool(
                "MuJetORT",
                "ORUtils::MuJetOverlapTool",
                BJetLabel=bjet_label,
                UseSlidingDR=boosted_leptons,
                **common_props,
            )

        if taus and electrons:
            tool.setPrivateTool(
                "TauEleORT", "ORUtils::DeltaROverlapTool", DR=0.2, **common_props
            )

        if taus and muons:
            tool.setPrivateTool(
                "TauMuORT", "ORUtils::DeltaROverlapTool", DR=0.2, **common_props
            )

        if taus and jets:
            tool.setPrivateTool(
                "TauJetORT", "ORUtils::DeltaROverlapTool", DR=0.2, **common_props
            )

        if photons and electrons:
            tool.setPrivateTool(
                "PhoEleORT", "ORUtils::DeltaROverlapTool", **common_props
            )

        if photons and muons:
            tool.setPrivateTool(
                "PhoMuORT", "ORUtils::DeltaROverlapTool", **common_props
            )

        if photons and jets:
            tool.setPrivateTool(
                "PhoJetORT", "ORUtils::DeltaROverlapTool", **common_props
            )

        if electrons and lrjets:
            tool.setPrivateTool(
                "EleFatJetORT", "ORUtils::DeltaROverlapTool", DR=1.0, **common_props
            )

        if jets and lrjets:
            tool.setPrivateTool(
                "JetFatJetORT", "ORUtils::DeltaROverlapTool", DR=1.0, **common_props
            )

    @property
    def produces_aux(self):
        return {
            k: {self._output_label, add_sys_pattern(self._output_label)}
            for k in self._inputs.values()
        }

    @property
    def produces_containers(self):
        return {}

    @property
    def requires_aux(self):
        deps = copy.deepcopy(self._extra_deps)
        for k in self._inputs.values():
            deps[k].add(self._input_label)
        return deps

    def requires_objects(
        self, required_output: Dict[Tuple[str, str], boolean.Expression]
    ) -> Dict[Tuple[str, str], boolean.Expression]:
        presel = algebra.parse(self._input_label)
        return {
            data: required_output.get(data, Node.PRESELECTED_OBJECTS) | presel
            for data in self.requires
        }

    @property
    def object_selections(self) -> Dict[str, Iterable[Selection]]:
        return {
            container: Selection(
                add_sys_pattern(self._output_label),
                count=None,
                previous=[self._input_label],
            )
            for container in self._inputs.values()
        }

    def _on_add(self, scheduler):
        super()._on_add(scheduler)
        for container, sel in self.object_selections.items():
            scheduler.add_selection(container, sel)

    def _on_remove(self, scheduler):
        for container, sel in self.object_selections.items():
            scheduler.rm_selection(container, sel.name)
        super()._on_remove(scheduler)

    class Config(NodeConfigBlock):
        """The configuration block for overlap removal"""

        def __init__(self):
            """Create the configuration block"""
            from cpalganalysis.electrons import ElectronConfigBlock
            from cpalganalysis.muons import MuonConfigBlock
            from cpalganalysis.photons import PhotonConfigBlock
            from cpalganalysis.taus import TauConfigBlock
            from cpalganalysis.jets import SmallRJetConfigBlock, LargeRJetConfigBlock

            super().__init__()
            self.add_option(
                "DoEleEleOR",
                False,
                type=bool,
                help="Do overlap removal between electrons",
            )
            self.add_option(
                "BoostedLeptons",
                False,
                type=bool,
                help="Use settings for boosted leptons",
            )
            self.add_option(
                "DoMuPFJetOR",
                False,
                type=bool,
                help="Setting to remove PFlow jets which are likely just muons",
            )
            self.add_option(
                "InputLabel",
                help="The input label for overlap removal",
            )
            self.add_option(
                "OutputLabel",
                default="passOR",
                help="The output label for OR",
            )
            self.add_option(
                "BJetLabel",
                required=False,
                help="The b-jet label to use when using b-jet aware OR",
            )
            self.add_option(
                "InputJets",
                help="The input jet collection to use",
                default=find_output_container_from_subconfig_type(
                    SmallRJetConfigBlock, required=False
                ),
                type=mk_optional(replace_sys_pattern),
            )
            self.add_option(
                "InputElectrons",
                help="The input electron collection to use",
                default=find_output_container_from_subconfig_type(
                    ElectronConfigBlock, required=False
                ),
                type=mk_optional(replace_sys_pattern),
            )
            self.add_option(
                "InputMuons",
                help="The input muon collection to use",
                default=find_output_container_from_subconfig_type(
                    MuonConfigBlock, required=False
                ),
                type=mk_optional(replace_sys_pattern),
            )
            self.add_option(
                "InputPhotons",
                help="The input photon collection to use",
                default=find_output_container_from_subconfig_type(
                    PhotonConfigBlock, required=False
                ),
                type=mk_optional(replace_sys_pattern),
            )
            self.add_option(
                "InputTaus",
                help="The input tau collection to use",
                default=find_output_container_from_subconfig_type(
                    TauConfigBlock, required=False
                ),
                type=mk_optional(replace_sys_pattern),
            )
            self.add_option(
                "InputLRJets",
                help="The input large-R jet collection to use",
                default=find_output_container_from_subconfig_type(
                    LargeRJetConfigBlock, required=False
                ),
                type=mk_optional(replace_sys_pattern),
            )

        def _create_nodes_kwargs(
            self, metadata: Dict[str, Any], calib_dep: str = "__CALIBRATION__", **kwargs
        ) -> Dict:
            kwargs = super()._create_nodes_kwargs(
                metadata, calib_dep=calib_dep, **kwargs
            )
            for object in "jet", "electron", "muon", "photon", "tau", "lrjet":
                kwargs.setdefault(f"{object}_calib_dep", calib_dep)
            return kwargs

        def _create_nodes(
            self,
            metadata: Dict,
            jet_calib_dep: str = "__CALIBRATION__",
            electron_calib_dep="__CALIBRATION__",
            muon_calib_dep="__CALIBRATION__",
            photon_calib_dep="__CALIBRATION__",
            tau_calib_dep="__CALIBRATION__",
            lrjet_calib_dep="__CALIBRATION__",
            **kwargs,
        ):
            return OverlapRemovalAlg(
                input_label=self["InputLabel"],
                output_label=self["OutputLabel"],
                bjet_label=self["BJetLabel"],
                jets=self["InputJets"],
                electrons=self["InputElectrons"],
                muons=self["InputMuons"],
                photons=self["InputPhotons"],
                taus=self["InputTaus"],
                lrjets=self["InputLRJets"],
                do_ele_ele=self["DoEleEleOR"],
                boosted_leptons=self["BoostedLeptons"],
                do_mu_pf_jet=self["DoMuPFJetOR"],
                jet_calib_dep=jet_calib_dep,
                electron_calib_dep=electron_calib_dep,
                muon_calib_dep=muon_calib_dep,
                photon_calib_dep=photon_calib_dep,
                tau_calib_dep=tau_calib_dep,
                lrjet_calib_dep=lrjet_calib_dep,
            )

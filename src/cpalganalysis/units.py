import copy
from numbers import Number
import hepunits
from functools import singledispatchmethod
import re


class CLHEPValue:
    def __init__(self, value: float, unit: str):
        self._unit = ""
        self.unit = unit
        self.value = value

    @property
    def unit(self) -> str:
        return self._unit

    @unit.setter
    def unit(self, value: str):
        if not hasattr(hepunits, value):
            raise ValueError(value)
        self._unit = value

    @property
    def clhep_value(self):
        return self.value * getattr(hepunits, self.unit)

    def get_as(self, unit: str):
        try:
            conversion = getattr(hepunits, unit)
        except AttributeError:
            raise ValueError(unit)
        return self.clhep_value / conversion

    def __str__(self):
        return f"{self._value} {self.unit}"


class CLHEPReader:

    pattern = re.compile(
        r"(?P<value>[-+]?(?:[0-9]*[.])?[0-9]+(?:[eE][-+]?\d+)?)\s*(?P<unit>\w+)?"
    )

    def __init__(self, unit: str):
        self._unit = ""
        self.unit = unit

    @property
    def unit(self) -> str:
        return self._unit

    @unit.setter
    def unit(self, value: str):
        if not hasattr(hepunits, value):
            raise ValueError(value)
        self._unit = value

    @singledispatchmethod
    def parse(self, value) -> CLHEPValue:
        raise TypeError(type(value))

    @parse.register
    def _(self, value: Number):
        return CLHEPValue(self.unit, float(value))

    @parse.register
    def _(self, value: str):
        match = self.pattern.fullmatch(value)
        if match is None:
            raise ValueError(value)
        unit = match.group("unit")
        return CLHEPValue(float(match.group("value")), unit if unit else self.unit)

    @parse.register(type(None))
    def _(self, _):
        return None

    @parse.register
    def _(self, value: CLHEPValue):
        return copy.copy(value)

    def __call__(self, value):
        return self.parse(value)

    @property
    def description(self) -> str:
        return self.unit


# Make sure that HEP values can be written out
from configblock.dictwriter import DictWriter


@DictWriter.convert_value.register
def convert_value(self, value: CLHEPValue):
    return str(value)

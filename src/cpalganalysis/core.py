"""General algorithms that don't fit into a single group"""

import boolean
from operator import attrgetter
from typing import Dict, Iterable, Tuple, Union
from cpalganalysis.units import CLHEPReader

from cpalgnodes.booleanalgebra import algebra
from cpalgnodes.node import Node, ContainerInfo
from cpalgnodes.algnode import SimpleAlgNode
from cpalganalysis.nodeconfigblock import NodeConfigBlock
from cpalgnodes.selection import Definition, Selection, SelectionAlgNode
from cpalgnodes.utils import DataType


class PtEtaSelectionAlg(SelectionAlgNode):
    """Algorithm to perform kinematic selections"""

    def __init__(
        self,
        name: str,
        container: str,
        selection_name: str,
        min_pt: float = 0,
        max_pt: float = 0,
        max_eta: float = 0,
        use_cluster_eta=False,
        crack_veto=False,
        add_calib_dep=None,
        calib_dep="__CALIBRATION__",
        **kwargs,
    ):
        """Create the kinematic selection alg

        Parameters
        ----------
        name: str
            The name of the algorithm
        container: str
            The nickname of the container
        selection_name: str
            The name of the selection
        min_pt: float
            Minimum pt to require (0 for no cut)
        max_pt: float
            Maximum pt to require (0 for no cut)
        max_eta: float
            Maximum |eta| to require (0 for no cut)
        use_cluster_eta: bool
            Whether or not to use the cluster eta (for egamma objects only)
        crack_veto: bool
            Whether to veto the calorimeter crack region
        add_calib_dep: bool
            Whether to add a dependency on the kinematic calibration. If None a sensible default
            will be chosen (False if the selection is only on eta with UseClusterEta set to True).
        calib_dep: str
            The name of the (fake) calibration data dependency.
        **kwargs
            Extra kwargs that will be passed onto the SelectionAlgNode __init__ method
        """
        if add_calib_dep is None:
            add_calib_dep = (
                min_pt > 0 or max_pt > 0 or (max_eta > 0 and not use_cluster_eta)
            )
        if not add_calib_dep:
            calib_dep = None
        self._calib_dep = calib_dep

        super().__init__(
            name=name,
            container=container,
            selection_name=selection_name,
            requires_aux={attrgetter("_calib_dep")},
            **kwargs,
        )
        count = 0
        for cut in (min_pt, max_pt, max_eta):
            if cut > 0:
                count += 1
        if use_cluster_eta:
            count += 2
        if crack_veto:
            count += 2
        self.selection_count = 2
        tool = self.setPrivateTool(
            "selectionTool",
            "CP::AsgPtEtaSelectionTool",
            minPt=min_pt,
            maxPt=max_pt,
            maxEta=max_eta,
            useClusterEta=use_cluster_eta,
        )
        if crack_veto:
            tool["etaGapLow"] = 1.37
            tool["etaGapHigh"] = 1.52


class FlagSelectionAlg(SelectionAlgNode):
    """Compute the AND of multiple selections"""

    def __init__(
        self,
        name: str,
        container: str,
        selection_name: str,
        input_selections: Iterable[Selection],
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        name: str
            The name of the algorithm
        container: str
            The nickname of the container
        selection_name: str
            The name of the selection to create
        input_selections: Iterable[Selection]
            List of input selections
        **kwargs
            Arguments forwarded to the superclass
        """
        super().__init__(
            name=name, container=container, selection_name=selection_name, **kwargs
        )
        self._input_selections = list(input_selections)
        self.setPrivateTool(
            "selectionTool",
            "CP::AsgFlagSelectionTool",
            selectionFlags=[sel.selection_prop_value for sel in input_selections],
        )

    @property
    def selection(self) -> Selection:
        """The selection produced by this node"""
        return Definition(self.selection_name, self._input_selections)

    def requires_objects(
        self, required_output: Dict[Tuple[str, str], boolean.Expression]
    ):
        container = self.input_container
        presel = required_output.get((container, self.selection_name), Node.ALL_OBJECTS)
        requires = {}
        for sel in self._input_selections:
            requires[(container, sel.name)] = presel
            presel &= algebra.parse(sel.name)
        return requires

    def _requires_aux(self):
        # Remove any 'as_char' or 'as_bits' information
        return super()._requires_aux() | {sel.name for sel in self._input_selections}


class ObjectCutFlowHistAlg(SimpleAlgNode):
    """Algorithm to produce a cutflow for an object definition"""

    def __init__(
        self,
        container: str,
        selection: str,
        subselections: str,
        priority: int = 1000,
        name: str = None,
        root_stream_name: str = "ANALYSIS",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container : str
            The container on which the selection is defined
        selection : str
            The selection whose cutflow is being constructed
        subselections : str
            The selections to plot on the cutflow
        priority : int, optional
            Hint to the scheduler to control where this runs, by default 1000
        name : str, optional
            The name of this algorithm
        root_stream_name : str
            The name of the output stream, by default ANALYSIS
        """
        # NB: Put a very high priority to force this to run as soon as possible
        if name is None:
            name = f"{container}{selection}CutflowAlg"
        super().__init__(
            type="CP::ObjectCutFlowHistAlg",
            name=name,
            container_property="input",
            container=container,
            priority=priority,
            suppress_preselection=True,
            histPattern = f"{container}{selection}Cutflow_%SYS%",
            RootStreamName=root_stream_name,
            **kwargs,
        )
        self._selection = selection
        self._selections = subselections

    def _requires_aux(self):
        return super()._requires_aux() | set(self._selections)

    @property
    def has_job_output(self) -> bool:
        return True

    def requires_objects(
        self, required_output: Dict[Tuple[str, str], boolean.Expression]
    ):
        container = self.input_container
        presel = required_output.get((container, self._selection), Node.ALL_OBJECTS)
        requires = {}
        for sel in self._selections:
            requires[(container, sel)] = presel
            presel &= algebra.parse(sel)
        return requires

    def create(self, container_info: ContainerInfo):
        algorithm = super().create(container_info)
        selections = []
        selection_counts = []
        info = container_info[self.input_container]
        for sel in self._selections:
            count = info.selection_counts[sel]
            if count is None:
                selections.append(f"{sel},as_char")
                selection_counts.append(1)
            else:
                selections.append(f"{sel},as_bits")
                selection_counts.append(count)
        algorithm["selection"] = selections
        algorithm["selectionNCuts"] = selection_counts
        return algorithm


class LeptonTrackSelectionAlg(SelectionAlgNode):
    """Select on d0 significance and z0 sin theta for electron/muon tracks"""

    def __init__(
        self,
        name: str,
        container: str,
        selection_name: str,
        d0sig: float = 0,
        z0SinTheta: float = 0,
        eventInfo: str = "EventInfo",
        primaryVertices: str = "PrimaryVertices",
        **kwargs,
    ):
        """Algorithm to select on lepton track d0sig and z0

        Parameters
        ----------
        name: str
            The name of the algorithm
        container: str
            The nickname of the container being selected on
        selection_name: str
            The selection to create
        d0sig: float
            The value of d0 significance to require. 0 means no selection
        z0SinTheta: float
            The value of z0sinTheta to require. 0 means no selection
        eventInfo: str
            The name of the event info container
        primaryVertices: str
            The name of the primary vertex container
        **kwargs:
            Extra kwargs to be forwarded to the superclass constructor
        """
        super().__init__(
            type="CP::AsgLeptonTrackSelectionAlg",
            name=name,
            container=container,
            selection_name=selection_name,
            maxD0Significance=d0sig,
            maxDeltaZ0SinTheta=z0SinTheta,
            eventInfo=eventInfo,
            primaryVertices=primaryVertices,
            **kwargs,
        )
        count = 1
        for val in (d0sig, z0SinTheta):
            if val > 0:
                count += 1
        self.selection_count = count

    @property
    def requires_aux(self):
        return super().requires_aux | {
            self["eventInfo"]: {},
            self["primaryVertices"]: {},
        }

    class Config(NodeConfigBlock):
        """Configuration for the lepton track selection"""

        def __init__(self):
            super().__init__()
            self.add_option(
                "D0Sig",
                default=0,
                type=float,
                help="Maximum value of d0 significance to require. 0 means no cut.",
            )
            self.add_option(
                "Z0SinTheta",
                default="0 mm",
                type=CLHEPReader("mm"),
                help="Maximum value of z0 sin theta to require. 0 means no cut.",
            )

        def _create_nodes(self, metadata: Dict, container: str, wp_name: str, **kwargs):
            return LeptonTrackSelectionAlg(
                name=f"{container}{wp_name}TrackSel",
                container=container,
                selection_name=f"{wp_name}TrackSel",
                d0sig=self["D0Sig"].clhep_value,
                z0SinTheta=self["Z0SinTheta"],
            )


class OriginalObjectLinkAlg(SimpleAlgNode):
    """Provide the original object Link decoration from a copy to its parent"""

    def __init__(self, container: str, base_container: str, **kwargs):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the container to set the links on
        base_container: str
            The original container to link to
        """
        super().__init__(
            type="CP::AsgOriginalObjectLinkAlg",
            name=f"{container}To{base_container}OOLAlg",
            container=container,
            baseContainerName=base_container,
            produces_aux={"originalObjectLink"},
            **kwargs,
        )

    @property
    def requires_aux(self):
        return super() | {self["baseContainerName"]: {}}


class PileupReweightingAlg(SimpleAlgNode):
    """Algorithm to apply the pileup reweighting"""

    def __init__(
        self,
        prw_config_files: Iterable[str],
        lumi_calc_files: Iterable[str],
        name: str = "PileupReweightingAlg",
        container: str = "EventInfo",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        prw_config_files: Iterable[str]
            The list of configuration files to use
        lumi_calc_files: Iterable[str]
            The list of lumi calc files
        name: str
            The name of the algorithm
        container: str
            The name of the container to decorate
        **kwargs
            Other kwargs are forwarded to the superclass
        """
        super().__init__(
            type="CP::PileupReweightingAlg",
            name=name,
            container_property="eventInfo",
            container=container,
            # TODO add the decoration to this list
            produces_aux={"RandomRunNumber"},
            **kwargs,
        )
        self.setPrivateTool(
            "pileupReweightingTool",
            "CP::PileupReweightTool",
            ConfigFiles=list(prw_config_files),
            LumiCalcFiles=list(lumi_calc_files),
        )

    @classmethod
    def find_prw_files(cls, period: str, dsid: int, sim_type: Union[str, DataType]):
        """Find the PRW config files and lumi calc files

        Parameters
        ----------
        period: str
            The MC period  (e.g. mc16d)
        dsid: int
            The MC DSID
        sim_type: Union[str, DataType]
            The type of simulation (e.g. fullsim or AFII)
        """
        # TODO:
        # - provide a check that the files actually exist in GroupData
        # - Handle the RPV ll behaviour in SUSYTools
        # - Handle the znunu bug (check if this is still a thing)
        # - jet/b-jet aware OR?
        # - Combined mode (i.e. reweight 2015-2018 together)
        # - Set the 2017/2018 actualMu file somehow?
        # - Make sure we  have the correct lumi calc files
        dsid_start = str(dsid)[:3]
        if isinstance(sim_type, DataType):
            sim_type = sim_type.name
        sim_type = sim_type.upper()
        if sim_type in ("MC", "FULLSIM"):
            sim_type = "FS"
        prw_files = [
            f"dev/PileupReweighting/share/DSID{dsid_start}xxx/pileup_{period}_dsid{dsid}_{sim_type}.root"
        ]
        lumi_calc_files = []
        if period == "mc16a":
            lumi_calc_files += [
                "GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
                "GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root",
            ]
        if period == "mc16d":
            prw_files.append(
                "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"
            )
            lumi_calc_files.append(
                "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"
            )
        elif period == "mc16e":
            prw_files.append("GoodRunsLists/data18_13TeV/20190219/purw.actualMu.root")
            lumi_calc_files.append(
                "GoodRunsLists/data18_13TeV/20190219/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"
            )
        return {"prw_config_files": prw_files, "lumi_calc_files": lumi_calc_files}

    class Config(NodeConfigBlock):
        """Configuration for PRW"""

        def _create_nodes(self, metadata: Dict, **kwargs):
            return PileupReweightingAlg(
                **PileupReweightingAlg.find_prw_files(
                    metadata["period"],
                    metadata["dsid"],
                    metadata["dataType"],
                )
            )

"""Base classes for config blocks for single object types (e.g. muons, electrons"""

from typing import Any, Callable, Dict, List, Optional, Set, Tuple, Type, Union
from cpalganalysis.nodeconfigblock import ContainerIOInfo, NodeConfigBlock
from cpalganalysis.units import CLHEPReader
from configblock.utils import mk_optional, wrap_deferred_func
from cpalgnodes.node import Node
from cpalgnodes.selection import (
    InefficiencyScaleFactorCombinationAlg,
    ScaleFactor,
    ScaleFactorAlg,
    Selection,
    SelectionAlgNode,
    SimpleScaleFactorCombinationAlg,
)
from cpalgnodes.utils import add_sys_pattern, replace_sys_pattern
from cpalganalysis.core import FlagSelectionAlg, PtEtaSelectionAlg


class ObjectSelectionConfigBlock(NodeConfigBlock):
    """Config block to create a single selection on an object"""

    @classmethod
    def default_sf_name(self, SelectionName: str):
        """Create the default scale factor name by adding 'SF' to the end of the selection name"""
        return SelectionName + "SF"

    @classmethod
    def default_out_of_valid_name(self, SFName: str):
        """Create the default out of validity name"""
        return "Invalid" + replace_sys_pattern(SFName)

    def __init__(
        self,
        default_sel_name,
        defines_sf: bool,
    ):
        """Create the selection configuration block

        Parameters
        ----------
        default
            The default value for the selection name. Can be any value that the corresponding
            parameter in the add_option function could take.
        defines_sf: bool
            Whether this configuration also defines a scale factor, in which case options for the
            scale factor and out of validity names will also be created
        """
        super().__init__()
        self._defines_sf = defines_sf
        self.add_option(
            "SelectionName",
            default=default_sel_name,
            help="The name to set for this selection",
            detail=0 if default_sel_name is None else 10,
        )
        if defines_sf:
            self.add_option(
                "SFName",
                default=wrap_deferred_func(self.default_sf_name),
                type=add_sys_pattern,
                help="The name to set for the scale factor",
                detail=10,
            )
            self.add_option(
                "OutOfValidName",
                default=wrap_deferred_func(self.default_out_of_valid_name),
                help="The name to set for the out fo validity decoration",
                detail=10,
            )


class ObjectDefinitionConfigBlock(NodeConfigBlock):
    """Config block to create a single named definition on the object

    Note that further base classes should not override the _create_nodes method and should instead
    override _create_selection_nodes
    """

    def __init__(
        self,
        use_cluster_eta: bool = False,
        add_crack_veto_opt: bool = False,
        order: List[str] = None,
    ):
        """Create the block

        Parameters
        ----------
        parent: ConfigBlock
            The parent of this block in the hierarchy
        use_cluster_eta: bool
            Whether to use the egamma cluster eta when performing eta selections
        add_crack_veto_opt: bool
            Whether to add an option to add the CrackVeto
        order: List[str]
            The order of selections in the final definition. Any missed here will be added in a
            an undetermined order
        """
        super().__init__()
        self.add_option(
            "MinPt",
            required=False,
            type=CLHEPReader("GeV"),
            help="Minimum pT to require",
        )
        self.add_option(
            "MaxEta",
            type=mk_optional(float),
            required=False,
            help="Maximum |eta| to require.",
        )
        if add_crack_veto_opt:
            self.add_option(
                "CrackVeto",
                default=True,
                type=bool,
                help="Whether to veto objects falling in the calorimeter crack region",
            )
        self.add_option(
            "DefinitionName",
            help="The name of the definition to create.",
            detail=10,
            set_from_key=True,
        )
        self.add_option(
            "Preselection",
            required=False,
            help="Selection to use as a preselection to this definition",
        )
        self._use_cluster_eta = use_cluster_eta
        self._order = order

    @property
    def order(self) -> List[str]:
        """The order of selections in the final definition.

        Any missed here will be added in an undetermined order
        """
        return self._order

    @order.setter
    def order(self, value: List[str]):
        self._order = value

    @classmethod
    def _get_sel_and_sf(
        cls, node: Union[Node, Dict[str, Node]]
    ) -> Tuple[Optional[Selection], Optional[ScaleFactor]]:
        """Get the selection and/or scale factor from a node or subset of nodes"""
        if isinstance(node, Node):
            return (
                node.selection if isinstance(node, SelectionAlgNode) else None,
                node.sf if isinstance(node, ScaleFactorAlg) else None,
            )
        else:
            return (
                next(
                    (
                        n.selection
                        for n in node.values()
                        if isinstance(n, SelectionAlgNode)
                    ),
                    None,
                ),
                next(
                    (n.sf for n in node.values() if isinstance(n, ScaleFactorAlg)),
                    None,
                ),
            )

    def _get_definition_order(
        self, nodes
    ) -> List[Tuple[Optional[Selection], Optional[ScaleFactor]]]:
        """Get the order of selections/SFs in the definition"""
        final_order = []
        # Add the preselection here. There's a slight hack here in that we assume that it is a
        # char variable
        if self["Preselection"]:
            final_order.append((Selection(self["Preselection"]), None))
        # Add any that are in the pre-specified order
        final_order += [
            self._get_sel_and_sf(nodes[key]) for key in self.order if key in nodes
        ]
        # Finally add any that are declared but not specified in the order
        final_order += [
            self._get_sel_and_sf(node)
            for key, node in nodes.items()
            if key not in self.order
        ]
        # Remove any that were neither scale factor nor selection
        return [x for x in final_order if x != (None, None)]

    def _create_definition_nodes(
        self,
        metadata: Dict,
        container: str,
        selections: List[Tuple[Optional[Selection], Optional[ScaleFactor]]],
    ):
        """Create the definition and scale factor nodes"""
        nodes = {
            "Sel": FlagSelectionAlg(
                f"{self['DefinitionName']}{container}SummarySelectionAlg",
                container=container,
                selection_name=self["DefinitionName"],
                input_selections=[sel for sel, _ in selections if sel is not None],
            )
        }
        if metadata["dataType"].is_simulation:
            sfs = [sf for _, sf in selections if sf is not None]
            if len(sfs):
                if any(sf.provides_inefficiency for sf in sfs):
                    nodes["SF"] = InefficiencyScaleFactorCombinationAlg(
                        container,
                        selection_name=self["DefinitionName"],
                        input_sfs=selections,
                    )
                else:
                    nodes["SF"] = SimpleScaleFactorCombinationAlg(
                        container,
                        selection_name=self["DefinitionName"],
                        input_sfs=sfs,
                    )
        return nodes

    def _create_nodes_kwargs(self, metadata: Dict[str, Any], **kwargs) -> Dict:
        new_kwargs = super()._create_nodes_kwargs(metadata, **kwargs)
        new_kwargs["definition_name"] = self["DefinitionName"]
        return new_kwargs

    def _create_selection_and_sf_nodes(
        self,
        metadata: Dict,
        container: str,
        definition_name: str,
        calib_dep: str = "__CALIBRATION__",
        **kwargs,
    ):
        """Create any extra nodes required for calculating selections and scale factors

        This is a slight hack necessary to make sure that all nodes providing selections and scale
        factors are available
        """
        nodes = {}
        min_pt = self["MinPt"].clhep_value if self["MinPt"] else 0
        max_eta = self["MaxEta"] if self["MaxEta"] else 0
        if self._use_cluster_eta:
            # Split the kinematic selection so that the eta selection can be done before calibration
            if max_eta > 0:
                nodes["Eta"] = PtEtaSelectionAlg(
                    name=f"{container}{definition_name}EtaSelectionAlg",
                    container=container,
                    selection_name=f"{definition_name}EtaSel",
                    max_eta=max_eta,
                    use_cluster_eta=True,
                    crack_veto=self.get("CrackVeto", False),
                )
            if min_pt > 0:
                nodes["Pt"] = PtEtaSelectionAlg(
                    name=f"{container}{definition_name}PtSelectionAlg",
                    container=container,
                    selection_name=f"{definition_name}PtSel",
                    min_pt=min_pt,
                    calib_dep=calib_dep,
                )
        elif min_pt > 0 or max_eta > 0:
            nodes["Kinematic"] = PtEtaSelectionAlg(
                name=f"{container}{definition_name}KinematicSelectionAlg",
                container=container,
                selection_name=f"{definition_name}KinematicSel",
                min_pt=min_pt,
                max_eta=max_eta,
                calib_dep=calib_dep,
                crack_veto=self.get("CrackVeto", False),
            )
        return nodes

    def _create_nodes(
        self,
        metadata: Dict,
        container: str,
        definition_name: str,
        calib_dep: str = "__CALIBRATION__",
        **kwargs,
    ):
        nodes = super()._create_nodes(
            metadata,
            container=container,
            definition_name=definition_name,
            calib_dep=calib_dep,
            **kwargs,
        )
        nodes.update(
            self._create_selection_and_sf_nodes(
                metadata,
                container=container,
                definition_name=definition_name,
                calib_dep=calib_dep,
                **kwargs,
            )
        )
        nodes["Definition"] = self._create_definition_nodes(
            metadata, container=container, selections=self._get_definition_order(nodes)
        )
        return nodes


class ObjectConfigBlock(NodeConfigBlock):
    """Overall configuration block for a single object type

    This block will create an OutputContainer option which is set from its key in the parent. It
    will also create an InputContainer option which gives the initial container to use. Its
    _create_nodes_kwargs method adds the 'container' kwarg which holds the name provided to the
    OutputContainer option.
    """

    def __init__(
        self,
        object_type: str,
        default_input_container: Union[
            str,
            Callable[[NodeConfigBlock], str],
        ] = None,
        default_subconfig_type: Type[NodeConfigBlock] = None,
    ):
        """Create the configuration block

        Parameters
        ----------
        object_type: str
            The type of the object - used to set the calib_dep keyword arguments
        default_input_container: str
            The name of the default input container for this block
        default_subconfig_type: str
            The default subconfig type to use (usually an ObjectSelectionConfigBlock)
        """
        super().__init__(default_subconfig_type=default_subconfig_type)
        self._object_type = object_type
        self.add_option(
            "OutputContainer",
            type=add_sys_pattern,
            help="The name of the container to create. If it does not contain '%SYS%' this pattern will be added",
            detail=10,
            set_from_key=True,
        )
        self.add_option(
            "InputContainer",
            default=default_input_container,
            help="The input container",
            detail=10,
        )

    @property
    def container(self) -> str:
        """The nickname for this container"""
        # Set the container nickname to be the output container without any %SYS% pattern
        return replace_sys_pattern(self["OutputContainer"])

    def _create_nodes_kwargs(
        self, metadata: Dict[str, Any], calib_dep: str = "__CALIBRATION__", **kwargs
    ) -> Dict:
        new_kwargs = super()._create_nodes_kwargs(
            metadata, calib_dep=calib_dep, **kwargs
        )
        new_kwargs["container"] = self.container
        # The logic here: If we have expressly provide an 'X_calib_dep' kwarg then we should
        # clearly use that. Otherwise we should fall back to calib_dep which defaults to
        # '__CALIBRATION__'
        key = f"{self._object_type}_calib_dep"
        if key in new_kwargs:
            new_kwargs["calib_dep"] = new_kwargs[key]
        return new_kwargs

    def _defined_containers(self) -> List[ContainerIOInfo]:
        return [
            ContainerIOInfo(
                nickname=self.container,
                input=self["InputContainer"],
                output=self["OutputContainer"],
            )
        ]

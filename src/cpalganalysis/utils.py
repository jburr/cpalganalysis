"""Helper functions for working with configuration blocks"""


import logging
from typing import Any, Type

from configblock.configblock import ConfigBlock
from configblock.utils import find_option_on_block_of_type


log = logging.getLogger(__name__)


def find_output_container_from_subconfig_type(
    type: Type[ConfigBlock],
    default: Any = None,
    required: bool = None,
    max_steps: int = -1,
    only_active: bool = True,
):
    """Make a function to retrieve the output container from a config of the specified type

    Parameters
    ----------
    type : Type[ConfigBlock]
        The type of block to search for
    default : Any, optional
        Default value if no matching block is found, by default None
    required : bool, optional
        Whether to raise an exception if no default is found. If left as None it will be set to
        True if the default value is None.
    max_steps : int, optional
        The number of steps up to allow. A value of 0 means that only the children of cfg are
        searched. A negative value guarantees searching the entire hierarchy. Defaults to -1
    only_active : bool, optional
        Only consider active blocks.
    """
    return find_option_on_block_of_type(
        type=type,
        key="OutputContainer",
        default=default,
        required=required,
        max_steps=max_steps,
        only_active=only_active,
        docstring=f"Search the hierarchy for the nearest {type} configuration block and return its output container",
    )

from cpalganalysis.electrons import ElectronConfigBlock
from cpalganalysis.jets import LargeRJetConfigBlock, SmallRJetConfigBlock
from cpalganalysis.met import MetConfigBlock
from cpalganalysis.muons import MuonConfigBlock
from cpalganalysis.nodeconfigblock import NodeConfigBlock
from cpalganalysis.overlapremoval import OverlapRemovalAlg
from cpalganalysis.photons import PhotonConfigBlock
from cpalganalysis.taus import TauConfigBlock


class AnalysisConfigBlock(NodeConfigBlock):
    """Simple example analysis"""

    def __init__(self):
        super().__init__(default_subconfig_type=AnalysisConfigBlock)
        self.add_subconfig_type("electrons", ElectronConfigBlock)
        self.add_subconfig_type("muons", MuonConfigBlock)
        self.add_subconfig_type("photons", PhotonConfigBlock)
        self.add_subconfig_type("taus", TauConfigBlock)
        self.add_subconfig_type("jets", SmallRJetConfigBlock)
        self.add_subconfig_type("lrjets", LargeRJetConfigBlock)
        self.add_subconfig_type("or", OverlapRemovalAlg.Config)
        self.add_subconfig_type("met", MetConfigBlock)

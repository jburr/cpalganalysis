"""Algorithm nodes for flavour-tagging selection"""

import enum
from operator import attrgetter
from typing import Dict, Union
from cpalganalysis.nodeconfigblock import NodeConfigBlock
from cpalganalysis.units import CLHEPReader
from cpalgnodes.selection import ScaleFactorAlg, SelectionAlgNode
from configblock.utils import find_option_on_block_of_type
from cpalganalysis.jets import JetCollection, SmallRJetConfigBlock

# TODO: track jets


def default_calib_file() -> str:
    """Get the default calibration file"""
    return "xAODBTaggingEfficiency/13TeV/2021-22-13TeV-MC16-CDI-2021-12-02_v2.root"


def default_min_pt(jet_collection: JetCollection) -> float:
    """Calculate the default minimum pT for b-tagging on a jet collection

    Raises
    ------
    ValueError
        The jet collection is not valid for b-tagging
    """
    if jet_collection.is_variable_radius:
        return 10e3
    elif jet_collection.radius == 4:
        return 20e3
    else:
        raise ValueError(f"Invalid collection {jet_collection} for b-tagging")


def make_wp_name(wp: int):
    """Form the correct working point name

    Parameters
    ----------
    wp: int
        Target efficiency of the working point as a percentage (60, 70, 77 or 85)
    """
    return f"FixedCutBEff_{wp}"


class FTagSelectionAlg(SelectionAlgNode):
    """Algorithm to select jets on b-tagging information"""

    class Tagger(enum.Enum):
        MV2c10 = enum.auto()
        DL1 = enum.auto()
        DL1r = enum.auto()

    def __init__(
        self,
        container: str,
        jet_collection: JetCollection,
        wp: Union[int, str],
        tagger: Tagger = Tagger.DL1r,
        min_pt: float = None,
        name: str = None,
        calib_file: str = default_calib_file(),
        calib_dep: str = "__CALIBRATION__",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the container to run over
        jet_collection: JetCollection
            The type of jet container to use
        wp: Union[int, str]
            The working point to use. Can either be a string or an integer. If it's a string it
            should be a complete working point definition, otherwise it should be an integer
            representing the target efficiency (either 60%, 70%, 77% or 85%)
        tagger: Tagger
            The tagging algorithm to use
        min_pt: float
            The minimum pT to set, if not provided a default based on the jet colllection will be
            used
        name: str
            The name of the algorithm
        calib_file: str
            The name of the calibration file to use
        calib_dep: str
            Dummy dependency to make sure that this runs after calibration
        **kwargs
            Extra keyword arguments are passed to the superclass
        """
        if isinstance(wp, int):
            wp = make_wp_name(wp)
        if min_pt is None:
            min_pt = default_min_pt(jet_collection)
        sel_name = tagger.name + wp
        if name is None:
            name = f"{container}{sel_name}FTagSelectionAlg"
        self._calib_dep = calib_dep
        super(FTagSelectionAlg, self).__init__(
            name=name,
            container=container,
            selection_name=sel_name,
            requires_aux={attrgetter("_calib_dep")},
            **kwargs,
        )
        self.setPrivateTool(
            "selectionTool",
            "BTaggingSelectionTool",
            TaggerName=tagger.name,
            OperatingPoint=wp,
            JetAuthor=str(jet_collection),
            FlvTagCutDefinitionsFileName=calib_file,
            MinPt=min_pt,
        )

    class Config(NodeConfigBlock):
        """The ftag selection configuration block"""

        def __init__(self):
            """Create the block"""
            super().__init__()
            self.add_enum_option(
                "Tagger",
                FTagSelectionAlg.Tagger,
                default=FTagSelectionAlg.Tagger.DL1r,
                help="The b-tagging algorithm to use",
            )
            self.add_option(
                "WP",
                help="The b-tagging working point. Can either be a full string description or a fixed target percentage integer (60, 70, 77 or 85)",
                set_from_single_value=True,
            )
            self.add_option(
                "MinPt",
                required=False,
                type=CLHEPReader("GeV"),
                help="The minimum pT for b-tagging",
            )
            self.add_enum_option(
                "SystematicsStrategy",
                FTagEfficiencyScaleFactorAlg.SystematicsStrategy,
                default=FTagEfficiencyScaleFactorAlg.SystematicsStrategy.Envelope,
                help="The systematics strategy to use",
                global_default="FTag/SystematicsStrategy",
            )
            self.add_option(
                "CalibrationFile",
                default_calib_file(),
                help="The calibration file to use",
                global_default="FTag/CDI",
            )
            self.add_option(
                "JetCollection",
                default=find_option_on_block_of_type(
                    SmallRJetConfigBlock, "JetCollection"
                ),
                help="The jet collection used",
                detail=10,
            )

        def _create_nodes(
            self,
            metadata: Dict,
            container: str,
            jet_collection: JetCollection,
            calib_dep: str = "__CALIBRATION__",
            **kwargs,
        ):
            nodes = {
                "Sel": FTagSelectionAlg(
                    container,
                    jet_collection,
                    wp=self["WP"],
                    tagger=self["Tagger"],
                    min_pt=self["MinPt"].clhep_value if self["MinPt"] else None,
                    calib_file=self["CalibrationFile"],
                    calib_dep=calib_dep,
                )
            }
            if metadata["dataType"].is_simulation:
                nodes["SF"] = FTagEfficiencyScaleFactorAlg(
                    container=container,
                    jet_collection=jet_collection,
                    wp=self["WP"],
                    tagger=self["Tagger"],
                    min_pt=self["MinPt"].clhep_value if self["MinPt"] else None,
                    calib_file=self["CalibrationFile"],
                    calib_dep=calib_dep,
                )
            return nodes


class FTagEfficiencyScaleFactorAlg(ScaleFactorAlg):
    """Algorithm to calculate scale factors for b-tagging"""

    # TODO: this is not used yet?
    class SystematicsStrategy(enum.Enum):
        """The strategy to use for b-tagging systematics"""

        SFEigen = enum.auto()
        Envelope = enum.auto()

    def __init__(
        self,
        container: str,
        jet_collection: JetCollection,
        wp: Union[int, str],
        tagger: FTagSelectionAlg.Tagger = FTagSelectionAlg.Tagger.DL1r,
        min_pt: float = None,
        sel_name: str = None,
        name: str = None,
        calib_file: str = default_calib_file(),
        calib_dep: str = "__CALIBRATION__",
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container: str
            The nickname of the container to run over
        jet_collection: JetCollection
            The type of jet container to use
        wp: Union[int, str]
            The working point to use. Can either be a string or an integer. If it's a string it
            should be a complete working point definition, otherwise it should be an integer
            representing the target efficiency (either 60%, 70%, 77% or 85%)
        tagger: Tagger
            The tagging algorithm to use
        min_pt: float
            The minimum pT to set, if not provided a default based on the jet colllection will be
            used
        sel_name: str
            The name of the selection whose scale factor to calculate
        name: str
            The name of the algorithm
        calib_file: str
            The name of the calibration file to use
        calib_dep: str
            Dummy dependency to make sure that this runs after calibration
        **kwargs
            Extra keyword arguments are passed to the superclass
        """
        if isinstance(wp, int):
            wp = make_wp_name(wp)
        if min_pt is None:
            min_pt = default_min_pt(jet_collection)
        if sel_name is None:
            sel_name = tagger.name + wp
        if name is None:
            name = f"{container}{sel_name}FTagEfficiencyAlg"
        self._calib_dep = calib_dep
        super().__init__(
            type="CP::BTaggingEfficiencyAlg",
            name=name,
            container_property="jets",
            container=container,
            sf_name=sel_name + "SF",
            requires_aux={attrgetter("_calib_dep")},
            provides_inefficiency=True,
            **kwargs,
        )
        self.setPrivateTool(
            "efficiencyTool",
            "BTaggingEfficiencyTool",
            TaggerName=tagger.name,
            OperatingPoint=wp,
            JetAuthor=str(jet_collection),
            ScaleFactorFileName=calib_file,
            MinPt=min_pt,
        )
